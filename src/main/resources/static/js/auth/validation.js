$(document).ready(function () {
    $("#submitButton").click(function () {
        event.preventDefault();
        if (checkSignInFields()) {
            checkLogin();
        }
    });

    $("#submitRegButton").click(function () {
        event.preventDefault();
        if (checkSignUpFields()) {
            checkRegInfo();
        }
    });

    function checkLogin() {
        let login = $("#username").val();
        let password = $("#password").val();

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "/user/checkUserLogin",
            data: JSON.stringify({
                'login': login,
                'password': password
            }),
            success: function (response) {
                if (response.login === 'no user found!') {
                    $("#submitButton").css("background-color", "red");
                    M.toast({html: response.login, classes: 'rounded'})
                } else if (response.login === 'user found!' && response.password === "input valid!") {
                    $(".submitForm").submit();
                } else {
                    M.toast({html: 'Invalid password', classes: 'rounded'})
                }
            },
            error: function (e) {
                console.log(e);
                $("#submitButton").css("background-color", "red");
                M.toast({html: 'Incorrect username or password!', classes: 'rounded'})
            }
        });
    }

    function checkRegInfo() {
        let login = $("#login").val();
        let email = $("#email").val();

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                'login': login,
                'email': email
            }),
            url: "/user/checkLoginAndEmail",
            success: function (response) {
                if (response.success === false) {
                    M.toast({html: response.message, classes: 'rounded'});
                } else {
                    $(".registrationForm").submit();
                }
            },
            error: function () {
                $("#submitRegButton").css("background-color", "red");
                M.toast({html: 'Invalid input', classes: 'rounded'});
            }
        });
    }

    function checkSignInFields() {
        if (document.getElementById('username').value.length < 6) {
            $("#submitButton").css("background-color", "red");
            M.toast({html: 'Login must be 6 digits or more', classes: 'rounded'});
            return false;
        } else if (document.getElementById('password').value.length < 4) {
            $("#submitButton").css("background-color", "red");
            M.toast({html: 'Password must be 4 digits or more', classes: 'rounded'});
            return false;
        } else {
            return true;
        }
    }

    function checkSignUpFields() {
        if (document.getElementById('login').value < 6) {
            $("#submitRegButton").css("background-color", "red");
            M.toast({html: 'Login must be 6 digits or more!', classes: 'rounded'});
            return false;
        } else if (document.getElementById('password').value < 4) {
            $("#submitRegButton").css("background-color", "red");
            M.toast({html: 'Password must be 4 digits or more', classes: 'rounded'});
            return false;
        } else if (document.getElementById('email').value < 7) {
            $("#submitRegButton").css("background-color", "red");
            M.toast({html: 'Enter valid email', classes: 'rounded'});
            return false;
        }
        return true;
    }
});