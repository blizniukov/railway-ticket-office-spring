$(document).ready(function () {
    $("#main-search-btn").click(function () {
        let date = document.getElementById('date-take').value;
        $('select').formSelect();
        event.preventDefault();
        if (document.getElementById('departure').selectedIndex === 0 || document.getElementById('destination').selectedIndex === 0) {
            M.toast({html: 'Select departure and destination', classes: 'rounded'});
        } else if (document.getElementById('departure').selectedIndex === document.getElementById('destination').selectedIndex) {
            M.toast({html: 'Departure and destination can`t be same', classes: 'rounded'});
        } else if (date === "") {
            M.toast({html: 'Select route date', classes: 'rounded'});
        } else {
            $(".search_form").submit();
        }
    });
});


