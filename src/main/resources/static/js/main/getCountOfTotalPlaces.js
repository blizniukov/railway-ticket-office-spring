function getWagonsPlaces(routeId, trainId) {

    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=UTF-8',
        dataType: 'json',
        url: "/routes/wagons/" + trainId,
        success: function (response) {
            let trainName = response.trainName;
            let types = response.types;

            $("div#footer-card-" + trainName).empty();

            for (let i = 0; i < types.length; i++) {
                let type = response.types[i];
                let link = '/routes/' + routeId + '/wagontype/' + type.name + '/seats';
                let footerBlock = "";
                if (type.count > 0) {
                    footerBlock = footerBlock.concat('<div class="wagon-type-wrap" onclick="location.href=' + "\x27" + link + "\x27" + ';">' +
                        '<span class="wagon-type">' + type.name + '</span>' +
                        '<span class="wagon-cost">' + type.cost + '₴' + '</span>' +
                        '<span class="wagon-count">' + type.count + ' seats' + '</span></div>');
                }

                $("div#footer-card-" + trainName).append(footerBlock);
            }

        },
        error: function (e) {
            console.log(e);
        }
    });
}