function getSeats(wagonId, wagonNumber) {
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: "/routes/wagons/" + wagonId + "/seats",
        success: function (data) {
            let seats = data.seats;
            $("div#seat-choose-wrap").empty();
            for (let i = 0; i < seats.length; i++) {
                let seat = data.seats[i];
                let status = seat.status;
                let seatBlock = '';
                let seatStat;

                if (status === 'FREE') {
                    seatStat = ' onclick="changeState(' + seat.id + ')"';
                } else {
                    seatStat = '';
                }

                if (seats.length > 0) {
                    seatBlock = seatBlock.concat(
                        '<span class="seat-show-' + status + '" id="seat-id-' + seat.id + '"' + seatStat + '>' + seat.seatNumber + '</span>');
                }
                $("div#seat-choose-wrap").append(seatBlock);
            }
        },

    });
    createStyles();
    addWagonNumberToOrderBlock(wagonNumber)
}

function createStyles() {
    document.getElementById('seat-select-area-wrapper').style.background = '#f3f3f3';
    document.getElementById('ticket-choose').style.display = 'block';
}

function addWagonNumberToOrderBlock(wagonNumber) {
    document.getElementById('wagon-number').innerText = wagonNumber;
    document.getElementById('seat-number').innerText = '';
    document.getElementById('book-ticket-seat').innerText = '';
    document.getElementById('book-ticket-route').innerText = '';
}