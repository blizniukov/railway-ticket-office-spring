let count = 0;
let style;

function changeState(id) {
    if (count === 0) {
        style = document.getElementById('seat-id-' + id).style.background;
        document.getElementById('seat-id-' + id).style.background = 'red';
        count = id;
    } else {
        document.getElementById('seat-id-' + count).style.background = style;
        style = document.getElementById('seat-id-' + id).style.background;
        document.getElementById('seat-id-' + id).style.background = 'red';
        count = id;
    }
    passDataToTicketBookForm(id);
    addDataToOrderBlock(id);
    showSelectedSeatBlock();
}

function passDataToTicketBookForm(id) {
    document.getElementById('book-ticket-seat').value = id;
}

function showSelectedSeatBlock() {
    document.getElementById('selected-seat').style.display = 'block';
}

function addDataToOrderBlock(id) {
    document.getElementById('seat-number').innerText =
        document.getElementById('seat-id-' + id).innerText;
}