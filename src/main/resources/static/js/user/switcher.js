function switchUserState(id) {
    let userSwitcher = document.getElementById("user-active-" + id).checked;
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=UTF-8',
        dataType: 'json',
        url: "/railway/admin/users/changeUserState/" + id + "/active/" + userSwitcher,
        error: function (e) {
            M.toast({html: e.responseText, classes: 'rounded'});
        }
    });
}