function passDataToUpdateModal(id, dep, dest) {
    let status = document.getElementById('route-status-' + id).innerText;

    document.getElementById('route-update-id').value = id;
    document.getElementById('route-update-name').value = document.getElementById('route-name-' + id).innerText;

    document.getElementById('route-update-status-' + status).selected = true;
    document.getElementById('route-update-departure-' + dep).selected = true;
    document.getElementById('route-update-destination-' + dest).selected = true;
    $('select').formSelect();
    document.getElementById('route-update-departure-' + dep).defaultSelected = false;
    document.getElementById('route-update-destination-' + dest).defaultSelected = false;
    document.getElementById('route-update-status-' + status).defaultSelected = false;
}