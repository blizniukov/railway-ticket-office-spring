function changeTicketStatus(id) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "/tickets/return?ticketId=" + id,
        success: function (response) {
            console.log(response);
            let successMsq = "Ticket was returned";
            let errorMsq = "Unable return ticket";

            if (response.message === errorMsq) {
                M.toast({html: errorMsq, classes: 'rounded'})
            } else if (response.message === successMsq) {
                document.getElementById('table_row_user_' + id).remove();
                M.toast({html: successMsq, classes: 'rounded'});
            }
        },
        error: function (e) {
            console.log(id);
            M.toast({html: 'Error Send Request!', classes: 'rounded'});
            console.log(e);
        }
    });
}

