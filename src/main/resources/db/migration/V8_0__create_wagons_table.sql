DROP TABLE IF EXISTS wagons CASCADE;

CREATE TABLE wagons
(
    id             BIGSERIAL PRIMARY KEY,
    number         BIGINT,
    type           VARCHAR(50) NOT NULL,
    count_of_seats BIGINT      NOT NULL,
    seat_cost      BIGINT      NOT NULL,
    train_id       BIGINT REFERENCES trains (id)
);
