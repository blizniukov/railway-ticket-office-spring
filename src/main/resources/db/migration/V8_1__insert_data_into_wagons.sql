INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (1, 'ECONOMY', 30, 320, 1);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (2, 'BUSINESS', 30, 1123, 1);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (3, 'STANDARD', 30, 560, 1);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (4, 'STANDARD', 30, 568, 1);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (5, 'ECONOMY', 30, 320, 1);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (6, 'BUSINESS', 30, 980, 1);

INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (1, 'STANDARD', 30, 758, 2);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (2, 'BUSINESS', 30, 1532, 2);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (3, 'STANDARD', 30, 758, 2);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (4, 'BUSINESS', 30, 1532, 2);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (5, 'ECONOMY', 30, 123, 2);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (6, 'ECONOMY', 30, 123, 2);

INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (1, 'ECONOMY', 30, 320, 3);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (2, 'STANDARD', 30, 560, 3);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (3, 'BUSINESS', 30, 1123, 3);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (4, 'STANDARD', 30, 560, 3);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (5, 'ECONOMY', 30, 320, 3);

INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (1, 'STANDARD', 30, 674, 4);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (2, 'ECONOMY', 30, 320, 4);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (3, 'ECONOMY', 30, 320, 4);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (4, 'BUSINESS', 30, 1345, 4);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (5, 'STANDARD', 30, 674, 4);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (6, 'BUSINESS', 30, 1345, 4);

INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (1, 'ECONOMY', 30, 320, 5);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (2, 'STANDARD', 30, 560, 5);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (3, 'ECONOMY', 30, 320, 5);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (4, 'BUSINESS', 30, 1123, 5);
INSERT INTO wagons(number, type, count_of_seats, seat_cost, train_id)
VALUES (5, 'STANDARD', 30, 560, 5);