INSERT INTO routes(name, departure_date, arrival_date, departure_id, destination_id, route_status, train_id)
VALUES ('ZD-12', '2019-04-20 19:00:00.000000', '2019-04-20 11:01:00.000000', 1, 2, 'PLANNED', 1);

INSERT INTO routes(name, departure_date, arrival_date, departure_id, destination_id, route_status, train_id)
VALUES ('BGH-23', '2019-04-20 12:00:00.000000', '2019-04-21 11:00:00.000000', 1, 2, 'PLANNED', 2);

INSERT INTO routes(name, departure_date, arrival_date, departure_id, destination_id, route_status, train_id)
VALUES ('FKV-222', '2019-04-20 11:00:00.000000', '2019-04-21 11:11:00.000000', 1, 2, 'PLANNED', 3);

INSERT INTO routes(name, departure_date, arrival_date, departure_id, destination_id, route_status, train_id)
VALUES ('MRG-323', '2019-04-20 13:00:00.000000', '2019-04-23 11:35:00.000000', 1, 2, 'IN_PROGRESS', 4);

INSERT INTO routes(name, departure_date, arrival_date, departure_id, destination_id, route_status, train_id)
VALUES ('SKDF-345', '2019-04-20 14:00:00.000000', '2019-04-24 11:34:00.000000', 1, 2, 'FINISHED', 5);

INSERT INTO routes(name, departure_date, arrival_date, departure_id, destination_id, route_status, train_id)
VALUES ('FHG-123', '2019-04-28 19:00:00.000000', '2019-04-29 12:23:00.000000', 2, 7, 'PLANNED', 6);