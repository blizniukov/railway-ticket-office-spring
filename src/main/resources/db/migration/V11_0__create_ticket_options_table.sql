DROP TABLE IF EXISTS ticket_options CASCADE;

CREATE TABLE ticket_options
(
    ticket_id BIGINT PRIMARY KEY REFERENCES tickets (id),
    option_id BIGINT REFERENCES options (id)
);
