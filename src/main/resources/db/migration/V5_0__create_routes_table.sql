DROP TABLE IF EXISTS routes CASCADE;

CREATE TABLE routes
(
    id             BIGSERIAL   NOT NULL PRIMARY KEY,
    name           VARCHAR(50) UNIQUE,
    departure_date TIMESTAMP,
    arrival_date   TIMESTAMP,
    departure_id   BIGINT      NOT NULL REFERENCES stations (id),
    destination_id BIGINT      NOT NULL REFERENCES stations (id),
    route_status   VARCHAR(50) NOT NULL,
    train_id       BIGINT REFERENCES trains (id)
);
