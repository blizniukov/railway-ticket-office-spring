DROP TABLE IF EXISTS users CASCADE;

CREATE TABLE users
(
    id       BIGSERIAL PRIMARY KEY,
    email    VARCHAR(50)  NOT NULL UNIQUE,
    login    VARCHAR(50)  NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    active   BOOLEAN      NOT NULL,
    role_id  BIGINT       NOT NULL REFERENCES roles (id)
);