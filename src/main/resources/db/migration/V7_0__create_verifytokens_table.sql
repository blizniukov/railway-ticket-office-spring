DROP TABLE IF EXISTS verifytokens CASCADE;

CREATE TABLE verifytokens
(
    id          BIGSERIAL PRIMARY KEY,
    token       VARCHAR(255) NOT NULL UNIQUE,
    user_id     BIGINT       NOT NULL UNIQUE REFERENCES users (id),
    expiry_date DATE         NOT NULL
);
