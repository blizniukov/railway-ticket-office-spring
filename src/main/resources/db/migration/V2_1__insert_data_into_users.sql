INSERT INTO users(email, login, password, active, role_id)
VALUES ('blizniukov@gmail.com', 'blizniukov', '$2a$10$uvpJV3FZ77YTHthEmkBtPONKb4G5dTmy9IKZfi6qP6ywArhx3gZzy', TRUE, 1);

INSERT INTO users(email, login, password, active, role_id)
VALUES ('test@gmail.com', 'kkkkkk', '$2a$10$uvpJV3FZ77YTHthEmkBtPONKb4G5dTmy9IKZfi6qP6ywArhx3gZzy', TRUE, 2);

INSERT INTO users(email, login, password, active, role_id)
VALUES ('google@gmail.com', 'google', '$2a$10$uvpJV3FZ77YTHthEmkBtPONKb4G5dTmy9IKZfi6qP6ywArhx3gZzy', TRUE, 2);

INSERT INTO users(email, login, password, active, role_id)
VALUES ('epam@gmail.com', 'epam', '$2a$10$uvpJV3FZ77YTHthEmkBtPONKb4G5dTmy9IKZfi6qP6ywArhx3gZzy', TRUE, 2);