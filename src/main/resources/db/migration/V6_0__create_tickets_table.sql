DROP TABLE IF EXISTS tickets CASCADE;

CREATE TABLE tickets
(
    id            BIGSERIAL   NOT NULL PRIMARY KEY,
    first_name    VARCHAR(30) NOT NULL,
    last_name     VARCHAR(30) NOT NULL,
    route_id      BIGINT      NOT NULL REFERENCES routes (id),
    user_id       BIGINT      NOT NULL REFERENCES users (id),
    wagon_id      BIGINT      NOT NULL,
    seat_id       BIGINT      NOT NULL,
    ticket_status VARCHAR(30) NOT NULL,
    cost          BIGINT      NOT NULL,
    pay_id        VARCHAR(200)
);
