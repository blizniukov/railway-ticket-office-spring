DROP TABLE IF EXISTS seats CASCADE;

CREATE TABLE seats
(
    id          BIGSERIAL PRIMARY KEY,
    seat_number BIGINT      NOT NULL,
    wagon_id    BIGINT      NOT NULL REFERENCES wagons (id),
    status      VARCHAR(30) NOT NULL
);
