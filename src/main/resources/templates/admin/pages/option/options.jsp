<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<c:choose>
    <c:when test="${sessionScope.lang == 'ukr'}">
        <fmt:setLocale value='ukr' scope="session"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value='en' scope="session"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="localization"/>
<html>
<head>
    <meta charset="UTF-8">
    <title>Options</title>
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/style.css" type="text/css"/>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<div class="header_fragment">
    <jsp:include page="/pages/fragments/header.jsp"/>
</div>
<div class="option_body_wrap">
    <div class="left_bar_fragment">
        <jsp:include page="/pages/fragments/leftbar.jsp"/>
    </div>
    <div class="container">
        <table id="options-table">
            <thead>
            <tr>
                <th class="center"><fmt:message key="options.name"/></th>
                <th class="center"><fmt:message key="options.price"/></th>
                <th class="center"><fmt:message key="operations"/></th>
            </tr>
            </thead>
            <tbody id="options-body" class="searchable">
            <%--@elvariable id="options" type="java"--%>
            <c:forEach items="${options}" var="option">
                <tr id="${option.id}">
                    <td class="center">${option.name}</td>
                    <td class="center">${option.price}</td>
                    <td class="operations center">
                        <form method="POST" class="delete-option-form-${option.id}"
                              action="${pageContext.request.contextPath}/admin/railway?action=optionDelete&id=${option.id}">
                            <button id="confirmOptionDel-${option.id}" type="submit"
                                    class="btn red"
                                    onclick="deleteOption(${option.id})"><i
                                    class="material-icons">delete</i>
                            </button>
                        </form>
                        <a class="btn blue modal-trigger" href="#updateOption"
                           onclick="passDataToUpdateModal('${option.id}', '${option.name}', '${option.price}')">
                            <i class="large material-icons">mode_edit</i>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red modal-trigger" href="#createOption">
                <i class="large material-icons">mode_edit</i>
            </a>
        </div>

        <!-- Confirmation delete modal -->
        <div id="modal1" class="modal">
            <div class="modal-content">
                <h4><fmt:message key="options.delete.confirmation"/></h4>
            </div>
            <div class="modal-footer">
                <a id="confirm-delete-modal" href="#"
                   class="modal-close waves-effect waves-green btn-flat"><fmt:message
                        key="yes"/></a>
                <a id="refuse-delete-modal" href="#"
                   class="modal-close waves-effect waves-green btn-flat"><fmt:message
                        key="no"/></a>
            </div>
        </div>

        <!-- Create Option Modal -->
        <div id="createOption" class="modal">
            <div class="modal-content">
                <h4><fmt:message key="option.create.modal.title"/></h4>
                <form action="${pageContext.request.contextPath}/admin/railway?action=optionCreate" class="submitOption"
                      method="post">
                    <input id="option-name" type="text" name="name"
                           placeholder="Name">
                    <input id="option-price" type="text" name="price"
                           placeholder="Price">
                    <input id="submitOption" type="submit" class="btn"
                           value="<fmt:message key="create_option.modal.submit"/>"/>
                </form>
            </div>
        </div>

        <!-- Update Option Modal -->
        <div id="updateOption" class="modal">
            <div class="modal-content">
                <h4><fmt:message key="option.update.modal.title"/></h4>
                <form action="${pageContext.request.contextPath}/admin/railway?action=optionUpdate"
                      class="updateOptionSubmit"
                      method="post">
                    <input id="option-update-id" type="hidden" name="id">
                    <input id="option-update-name" type="text" name="name"
                           placeholder="Name">
                    <input id="option-update-price" type="text" name="price"
                           placeholder="Price">
                    <input id="updateOptionSubmit" type="submit" class="btn"
                           value="<fmt:message key="update_option.modal.submit"/>"/>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="footer_fragment">
    <jsp:include page="/pages/fragments/footer.jsp"/>
</div>
<div id='message' class='show-message'>${param.get("message")}</div>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/showMessage.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/bin/materialize.min.js"></script>
<script>
    $(document).ready(function () {
        $('.fixed-action-btn').floatingActionButton();
        $('.modal').modal();
    });

    function deleteOption(id) {
        event.preventDefault();
        $('#modal1').modal('open');

        $("#confirm-delete-modal").click(function () {
            $('.delete-option-form-' + id).submit();
        });
    }

    function passDataToUpdateModal(id, name, price) {
        document.getElementById('option-update-id').value = id;
        document.getElementById('option-update-name').value = name;
        document.getElementById('option-update-price').value = price;
    }
</script>
</body>
</html>