<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<c:choose>
    <c:when test="${sessionScope.lang == 'ukr'}">
        <fmt:setLocale value='ukr' scope="session"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value='en' scope="session"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="localization"/>
<html>
<head>
    <meta charset="UTF-8">
    <title>Wagons</title>
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/style.css" type="text/css"/>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<div class="header_fragment">
    <jsp:include page="/pages/fragments/header.jsp"/>
</div>
<div class="wagon_body_wrap">
    <div class="left_bar_fragment">
        <jsp:include page="/pages/fragments/leftbar.jsp"/>
    </div>
    <div class="container">
        <table id="wagons-table">
            <thead>
            <tr>
                <th class="center">Type</th>
                <th class="center">Count of Seats</th>
                <th class="center">Seat Cost</th>
                <th class="center">Current train</th>
                <th class="center">Position in train</th>
                <th class="center"><fmt:message key="operations"/></th>
            </tr>
            </thead>
            <tbody id="books-body" class="searchable">
            <%--@elvariable id="wagons" type="java"--%>
            <c:forEach items="${wagons}" var="wagon">
                <tr id="${wagon.id}">
                    <td class="center">${wagon.type}</td>
                    <td class="center">${wagon.countOfSeats}</td>
                    <td class="center">${wagon.seatCost}</td>
                    <td class="center">${wagon.train.name}</td>
                    <td class="center">${wagon.number}</td>
                    <td class="operations center">
                        <form method="POST" class="delete-wagon-form-${wagon.id}"
                              action="${pageContext.request.contextPath}/admin/railway?action=wagonDelete&id=${wagon.id}">
                            <c:choose>
                                <c:when test="${not empty wagon.train.name}">
                                    <button type="submit" class="btn disabled"><i
                                            class="material-icons">delete</i>
                                    </button>
                                </c:when>
                                <c:otherwise>
                                    <button id="confirmWagonDel-${wagon.id}" type="submit"
                                            class="btn red"
                                            onclick="deleteWagon(${wagon.id})"><i
                                            class="material-icons">delete</i>
                                    </button>
                                </c:otherwise>
                            </c:choose>
                        </form>
                        <a class="btn blue modal-trigger" href="#updateWagon"
                           onclick="passDataToUpdateModal('${wagon.id}', '${wagon.type}', '${wagon.countOfSeats}',
                                   '${wagon.seatCost}', '${wagon.number}')">
                            <i class="large material-icons">mode_edit</i>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red modal-trigger" href="#createWagon">
                <i class="large material-icons">mode_edit</i>
            </a>
        </div>

        <!-- Confirmation delete modal -->
        <div id="modal1" class="modal">
            <div class="modal-content">
                <h4>Do you really want to delete Wagon</h4>
            </div>
            <div class="modal-footer">
                <a id="confirm-delete-modal" href="#"
                   class="modal-close waves-effect waves-green btn-flat"><fmt:message
                        key="yes"/></a>
                <a id="refuse-delete-modal" href="#"
                   class="modal-close waves-effect waves-green btn-flat"><fmt:message
                        key="no"/></a>
            </div>
        </div>

        <!-- Create Wagon Modal -->
        <div id="createWagon" class="modal">
            <div class="modal-content">
                <h4>Create Wagon</h4>
                <form action="${pageContext.request.contextPath}/admin/railway?action=wagonCreate" class="createWagon" method="post">
                    <select name="wagonType">
                        <option value="" disabled selected>Choose wagon type</option>
                        <option id="wagon-create-type-ECONOMY" value="ECONOMY">ECONOMY</option>
                        <option id="wagon-create-type-STANDARD" value="STANDARD">STANDARD</option>
                        <option id="wagon-create-type-BUSINESS" value="BUSINESS">BUSINESS</option>
                    </select>
                    <input id="wagon-create-count" type="text" name="wagonCountOfSeats" placeholder="Count of seats">
                    <input id="wagon-create-cost" type="text" name="wagonSeatCost" placeholder="Seat cost">
                    <input id="wagon-create-number" type="text" name="wagonNumber" placeholder="Number">
                    <input id="createWagonSubmit" type="submit" class="btn"
                           value="Create Wagon"/>
                </form>
            </div>
        </div>

        <!-- Update Wagon Modal -->
        <div id="updateWagon" class="modal">
            <div class="modal-content">
                <h4>Update Wagon</h4>
                <form action="${pageContext.request.contextPath}/admin/railway?action=WagonEdit&from=wagons" class="updateTrainSubmit"
                      method="post">
                    <input id="wagon-update-id" type="hidden" name="wagonId">
                    <select name="wagonType">
                        <option value="" disabled selected>Choose wagon type</option>
                        <option id="wagon-update-type-ECONOMY" value="ECONOMY">ECONOMY</option>
                        <option id="wagon-update-type-STANDARD" value="STANDARD">STANDARD</option>
                        <option id="wagon-update-type-BUSINESS" value="BUSINESS">BUSINESS</option>
                    </select>
                    <input id="wagon-update-count" type="text" name="wagonCountOfSeats" placeholder="Count of seats">
                    <input id="wagon-update-cost" type="text" name="wagonSeatCost" placeholder="Seat cost">
                    <input id="wagon-update-number" type="text" name="wagonNumber" placeholder="Position in train">
                    <input id="updateWagonSubmit" type="submit" class="btn"
                           value="<fmt:message key="update_train.modal.submit"/>"/>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="footer_fragment">
    <jsp:include page="/pages/fragments/footer.jsp"/>
</div>
<div id='message' class='show-message'>${param.get("message")}</div>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/showMessage.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/bin/materialize.min.js"></script>
<script>
    $(document).ready(function () {
        $('.fixed-action-btn').floatingActionButton();
        $('.modal').modal();
        $('select').formSelect();
    });

    function deleteWagon(id) {
        event.preventDefault();
        $('#modal1').modal('open');

        $("#confirm-delete-modal").click(function () {
            $('.delete-wagon-form-' + id).submit();
        });
    }

    function passDataToUpdateModal(id, type, countOfSeats, seatCost, number) {
        document.getElementById('wagon-update-id').value = id;
        document.getElementById('wagon-update-number').value = number;
        document.getElementById('wagon-update-type-' + type).selected = true;
        document.getElementById('wagon-update-count').value = countOfSeats;
        document.getElementById('wagon-update-cost').value = seatCost;
        $('select').formSelect();
        document.getElementById('wagon-update-type-' + type).defaultSelected = false;
    }
</script>
</body>
</html>