<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<c:choose>
    <c:when test="${sessionScope.lang == 'ukr'}">
        <fmt:setLocale value='ukr' scope="session"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value='en' scope="session"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="localization"/>
<html>
<head>
    <meta charset="UTF-8">
    <title>Train Edit</title>
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/style.css" type="text/css"/>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<div class="header_fragment">
    <jsp:include page="/pages/fragments/header.jsp"/>
</div>
<div class="train_body_wrap">
    <div class="left_bar_fragment">
        <jsp:include page="/pages/fragments/leftbar.jsp"/>
    </div>
    <div class="container">
<%--@elvariable id="train" type="java"--%>
<H5>Train ${train.name}</H5>
<div id="id-holder" style="display: none">${train.id}</div>
<table id="trains-table">
    <thead>
    <tr>
        <th class="center">Number</th>
        <th class="center">Type</th>
        <th class="center">Count of seats</th>
        <th class="center">Seat cost</th>
        <th class="center"><fmt:message key="operations"/></th>
    </tr>
    </thead>
    <tbody id="wagons-body" class="searchable">
    <%--@elvariable id="train.wagons" type="java"--%>
    <c:forEach items="${train.wagons}" var="wagon">
        <tr id="${wagon.id}">
            <td class="center">${wagon.number}</td>
            <td class="center">${wagon.type}</td>
            <td class="center">${wagon.countOfSeats}</td>
            <td class="center">${wagon.seatCost}</td>
            <td class="operations center">
                <form method="POST" class="delete-train-form-${wagon.id}"
                      action="${pageContext.request.contextPath}/railway?action=trainRemoveWagon&train=${train.id}&id=${wagon.id}">
                    <button id="confirmTrainDel-${wagon.id}" type="submit"
                            class="btn red"
                            onclick="deleteTrain(${wagon.id})"><i
                            class="material-icons">delete</i>
                    </button>
                </form>
                <a class="btn blue modal-trigger" href="#updateTrain"
                   onclick="passDataToUpdateModal('${wagon.id}', '${wagon.number}' , '${wagon.type}', '${wagon.countOfSeats}',
                           '${wagon.seatCost}')">
                    <i class="large material-icons">mode_edit</i>
                </a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red modal-trigger" href="#createTrain">
        <i class="large material-icons">mode_edit</i>
    </a>
</div>

<!-- Confirmation delete modal -->
<div id="modal1" class="modal">
    <div class="modal-content">
        <h4><fmt:message key="trains.delete.confirmation"/></h4>
    </div>
    <div class="modal-footer">
        <a id="confirm-delete-modal" href="#"
           class="modal-close waves-effect waves-green btn-flat"><fmt:message
                key="yes"/></a>
        <a id="refuse-delete-modal" href="#"
           class="modal-close waves-effect waves-green btn-flat"><fmt:message
                key="no"/></a>
    </div>
</div>

<!-- Add wagon to Train Modal -->
<div id="createTrain" class="modal">
    <div class="modal-content">
        <h4>Add Wagon</h4>
        <form action="${pageContext.request.contextPath}/admin/railway?action=trainAddWagon" class="submitTrain" method="post">
            <input id="add_wagon_to_train_id" type="hidden" name="id"
                   placeholder="Add Wagon to train">
            <select name="wagonId">
                <option id="routes-choose" value="" disabled selected>Select wagon</option>
                <%--@elvariable id="wagons" type="java"--%>
                <c:forEach items="${wagons}" var="wagon">
                    <option value="${wagon.id}">Type: ${wagon.type}, Count of seats: ${wagon.countOfSeats}, Seat
                        cost: ${wagon.seatCost}</option>
                </c:forEach>
            </select>
            <input id="submitTrain" type="submit" class="btn"
                   value="<fmt:message key="create_train.modal.submit"/>"/>
        </form>
    </div>
</div>

<!-- Update Wagon Modal -->
<div id="updateTrain" class="modal">
    <div class="modal-content">
        <h4>Update Wagon</h4>
        <form action="${pageContext.request.contextPath}/admin/railway?action=wagonEdit&from=single"
              class="updateTrainSubmit"
              method="post">
            <input id="wagon-update-id" type="hidden" name="wagonId">
            <input id="wagon-update-number" type="text" name="wagonNumber" placeholder="Number">
            <select name="wagonType">
                <option value="" disabled selected>Choose wagon type</option>
                <option id="wagon-update-type-ECONOMY" value="ECONOMY">ECONOMY</option>
                <option id="wagon-update-type-STANDARD" value="STANDARD">STANDARD</option>
                <option id="wagon-update-type-BUSINESS" value="BUSINESS">BUSINESS</option>
            </select>
            <input id="wagon-update-count" type="text" name="wagonCountOfSeats" placeholder="Count of seats">
            <input id="wagon-update-cost" type="text" name="wagonSeatCost" placeholder="Seat cost">
            <input id="updateWagonSubmit" type="submit" class="btn"
                   value="<fmt:message key="update_train.modal.submit"/>"/>
        </form>
    </div>
</div>
    </div>
</div>
<div class="footer_fragment">
    <jsp:include page="/pages/fragments/footer.jsp"/>
</div>
<div id='message' class='show-message'>${param.get("message")}</div>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/showMessage.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/bin/materialize.min.js"></script>
<script>
    $(document).ready(function () {
        $('.fixed-action-btn').floatingActionButton();
        $('.modal').modal();
        $('.datepicker').datepicker({
            container: 'body',
            format: "yyyy-mm-dd"
        });
        $('.timepicker').timepicker({
            container: 'body',
            twelveHour: false
        });
        $('select').formSelect();

        document.getElementById('add_wagon_to_train_id').value = document.getElementById('id-holder').innerText;
    });

    function deleteTrain(id) {
        event.preventDefault();
        $('#modal1').modal('open');

        $("#confirm-delete-modal").click(function () {
            $('.delete-train-form-' + id).submit();
        });
    }

    function passDataToUpdateModal(id, number, type, count, cost) {
        document.getElementById('wagon-update-id').value = id;
        document.getElementById('wagon-update-number').value = number;
        document.getElementById('wagon-update-type-' + type).selected = true;
        document.getElementById('wagon-update-count').value = count;
        document.getElementById('wagon-update-cost').value = cost;
        $('select').formSelect();
        document.getElementById('wagon-update-type-' + type).defaultSelected = false;
    }
</script>
</body>
</html>