<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<c:choose>
    <c:when test="${sessionScope.lang == 'ukr'}">
        <fmt:setLocale value='ukr' scope="session"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value='en' scope="session"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="localization"/>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trains</title>
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/style.css" type="text/css"/>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<div class="header_fragment">
    <jsp:include page="/pages/fragments/header.jsp"/>
</div>
<div class="train_body_wrap">
    <div class="left_bar_fragment">
        <jsp:include page="/pages/fragments/leftbar.jsp"/>
    </div>
    <div class="container">
        <table id="trains-table">
            <thead>
            <tr>
                <th class="center"><fmt:message key="trains.name"/></th>
                <th class="center">Type</th>
                <th class="center">Wagons</th>
                <th class="center"><fmt:message key="operations"/></th>
            </tr>
            </thead>
            <tbody id="books-body" class="searchable">
            <%--@elvariable id="trains" type="java"--%>
            <c:forEach items="${trains}" var="train">
                <tr id="${train.id}">
                    <td class="center"><a
                            href="${pageContext.request.contextPath}/admin/railway?action=trainSingle&id=${train.id}">${train.name}</a>
                    </td>
                    <td class="center">${train.type}</td>
                    <td class="center">${fn:length(train.wagons)}</td>
                    <td class="operations center">
                        <form method="POST" class="delete-train-form-${train.id}"
                              action="${pageContext.request.contextPath}/admin/railway?action=trainDelete&id=${train.id}">
                            <c:choose>
                                <c:when test="${fn:length(train.wagons)>0}">
                                    <button type="submit" class="btn disabled"><i
                                            class="material-icons">delete</i>
                                    </button>
                                </c:when>
                                <c:otherwise>
                                    <button id="confirmTrainDel-${train.id}" type="submit"
                                            class="btn red"
                                            onclick="deleteTrain(${train.id})"><i
                                            class="material-icons">delete</i>
                                    </button>
                                </c:otherwise>
                            </c:choose>
                        </form>
                        <a class="btn blue modal-trigger" href="#updateTrain"
                           onclick="passDataToUpdateModal('${train.id}', '${train.name}', '${train.type}')">
                            <i class="large material-icons">mode_edit</i>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red modal-trigger" href="#createTrain">
                <i class="large material-icons">mode_edit</i>
            </a>
        </div>

        <!-- Confirmation delete modal -->
        <div id="modal1" class="modal">
            <div class="modal-content">
                <h4><fmt:message key="trains.delete.confirmation"/></h4>
            </div>
            <div class="modal-footer">
                <a id="confirm-delete-modal" href="#"
                   class="modal-close waves-effect waves-green btn-flat"><fmt:message
                        key="yes"/></a>
                <a id="refuse-delete-modal" href="#"
                   class="modal-close waves-effect waves-green btn-flat"><fmt:message
                        key="no"/></a>
            </div>
        </div>

        <!-- Create Train Modal -->
        <div id="createTrain" class="modal">
            <div class="modal-content">
                <h4><fmt:message key="train.create.modal.title"/></h4>
                <form action="${pageContext.request.contextPath}/admin/railway?action=trainCreate" class="submitTrain" method="post">
                    <input id="train-name-create" type="text" name="name"
                           placeholder="<fmt:message key="trains.placeholder.name"/>">
                    <select name="type">
                        <option value="" disabled selected>Choose wagon type</option>
                        <option id="train-create-type-ECONOMY" value="DEFAULT">DEFAULT</option>
                        <option id="train-create-type-STANDARD" value="FAST">FAST</option>
                        <option id="train-create-type-BUSINESS" value="NIGHT">NIGHT</option>
                    </select>
                    <input id="submitTrain" type="submit" class="btn"
                           value="<fmt:message key="create_train.modal.submit"/>"/>
                </form>
            </div>
        </div>

        <!-- Update Train Modal -->
        <div id="updateTrain" class="modal">
            <div class="modal-content">
                <h4><fmt:message key="train.update.modal.title"/></h4>
                <form action="${pageContext.request.contextPath}/admin/railway?action=trainUpdate" class="updateTrainSubmit"
                      method="post">
                    <input id="train-update-id" type="hidden" name="id">
                    <input id="train-update-name" type="text" name="name"
                           placeholder="<fmt:message key="trains.placeholder.name"/>">
                    <select name="type">
                        <option value="" disabled selected>Choose wagon type</option>
                        <option id="train-update-type-ECONOMY" value="DEFAULT">DEFAULT</option>
                        <option id="train-update-type-STANDARD" value="FAST">FAST</option>
                        <option id="train-update-type-BUSINESS" value="NIGHT">NIGHT</option>
                    </select>
                    <input id="updateTrainSubmit" type="submit" class="btn"
                           value="<fmt:message key="update_train.modal.submit"/>"/>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="footer_fragment">
    <jsp:include page="/pages/fragments/footer.jsp"/>
</div>
<div id='message' class='show-message'>${param.get("message")}</div>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/showMessage.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/static/js/bin/materialize.min.js"></script>
<script>
    $(document).ready(function () {
        $('.fixed-action-btn').floatingActionButton();
        $('.modal').modal();
        $('.datepicker').datepicker({
            container: 'body',
            format: "yyyy-mm-dd"
        });
        $('.timepicker').timepicker({
            container: 'body',
            twelveHour: false
        });
        $('select').formSelect();
    });

    function deleteTrain(id) {
        event.preventDefault();
        $('#modal1').modal('open');

        $("#confirm-delete-modal").click(function () {
            $('.delete-train-form-' + id).submit();
        });
    }

    function passDataToUpdateModal(id, name, type) {
        document.getElementById('train-update-id').value = id;
        document.getElementById('train-update-name').value = name;
        document.getElementById('train-update-type-' + type).selected = true;
        $('select').formSelect();
        document.getElementById('train-update-type-' + type).defaultSelected = false;
    }
</script>
</body>
</html>