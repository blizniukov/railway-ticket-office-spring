package com.rto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.rto.model.repository")
@EntityScan(basePackages = "com.rto.model.entity")
@EnableScheduling
public class RailwayTicketOfficeApplication {

    public static void main(String[] args) {
        SpringApplication.run(RailwayTicketOfficeApplication.class, args);
    }
}
