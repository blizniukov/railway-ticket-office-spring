package com.rto.model.util.sendgrid;

import com.rto.model.entity.User;
import com.rto.model.entity.VerifyToken;
import com.sendgrid.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author Nikita Blyzniukov
 */
@Log4j
@Component
public class EmailSender {

    @Value("${spring.sendgrid.api-key}")
    private String apiKey;
    @Value("${spring.sendgrid.from}")
    private String fromEmail;

    private void sendHTML(final String to, final String message, final String title) {
        Content content = new Content("text/html", message);
        sendEmail(to, title, content);
    }

    public void send(final String to, final String message, final String title) {
        Content content = new Content("text/plain", message);
        sendEmail(to, title, content);
    }

    public void sendConfirmationEmail(User user, VerifyToken verifyToken) {
        String generateValidationMessage = generateValidationMessage(verifyToken.getToken());
        sendHTML(user.getEmail(), generateValidationMessage, "Welcome to RTO, confirm registration");
    }

    private void sendEmail(final String to, final String title, Content content) {
        Email from = new Email(fromEmail);
        Email toEmail = new Email(to);
        Mail mailSend = new Mail(from, title, toEmail, content);
        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mailSend.build());
            SendGrid sendGrid = new SendGrid(apiKey);
            sendGrid.api(request);
        } catch (IOException e) {
            log.error("Error send email", e);
        }
    }

    private static String generateValidationMessage(String token) {
        return "<form method=\"POST\" action=\"http://localhost:8080/auth/confirmRegistration\">" +
                "<input type=\"hidden\" value=\"" + token + "\" name=\"token\" hidden/>" +
                "<input type=\"submit\" value=\"Confirm\" name=\"token_button\"/></form>";
    }
}
