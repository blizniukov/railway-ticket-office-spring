package com.rto.model.util.pagination;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mykyta Blyzniukov
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Paginator {

    public static void paginate(Model model, Page page) {
        List<Sort.Order> sort = page.getSort().stream().collect(Collectors.toList());
        if (!sort.isEmpty()) {
            Sort.Order order = sort.get(0);
            model.addAttribute("sortProp", order.getProperty());
            model.addAttribute("sortDesc", order.getDirection() == Sort.Direction.DESC);
        }
    }
}
