package com.rto.model.util.scheduler;

import com.rto.model.entity.*;
import com.rto.model.service.RouteService;
import com.rto.model.service.SeatService;
import com.rto.model.service.VerifyTokenService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Component
@Log4j
@AllArgsConstructor
public class ScheduledTasks {

    private VerifyTokenService verifyTokenService;
    private RouteService routeService;
    private SeatService seatService;

    @Scheduled(fixedRate = 86400000)
    public void clearExpiredTokens() {
        List<VerifyToken> tokensToDelete = verifyTokenService.getAllExpiresBeforeDate(LocalDate.now());
        verifyTokenService.deleteAll(tokensToDelete);
        log.info("Scheduler delete EXPIRED Verify Tokens");
    }

    @Scheduled(fixedRate = 21600000)
    public void changePassedRoutesStateAndTicketAndSeat() {
        List<Route> finishedRoutes = routeService.getAllFinishedRoutesBeforeDate(LocalDateTime.now());
        finishedRoutes.forEach(x -> x.setRouteStatus(RouteStatus.FINISHED));
        routeService.updateAll(finishedRoutes);

        log.info("Scheduler change state of FINISHED Routes, Tickets and Seats");
    }

    @Scheduled(fixedRate = 900000)
    public void changePendingSeatsState() {
        List<Seat> pendingSeats = seatService.getAllByStatus(SeatStatus.PENDING);
        pendingSeats.forEach(x -> x.setStatus(SeatStatus.FREE));
        seatService.updateAll(pendingSeats);
        log.info("Scheduler change state of PENDING Seats to FREE");
    }
}
