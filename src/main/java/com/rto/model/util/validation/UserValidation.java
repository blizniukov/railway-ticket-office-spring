package com.rto.model.util.validation;

import com.rto.model.dto.AjaxValidationUser;
import com.rto.model.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Mykyta Blyzniukov
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserValidation {

    public static boolean isLoginBlank(String login) {
        return StringUtils.isBlank(login);
    }

    public static String validatePassword(BCryptPasswordEncoder passwordEncoder,
                                          AjaxValidationUser validationUserBody,
                                          User userFromDb) {
        return StringUtils.isBlank(validationUserBody.getPassword()) ||
                !passwordEncoder.matches(validationUserBody.getPassword(),
                        userFromDb.getPassword())
                ? "invalid input!" : "input valid!";
    }
}
