package com.rto.model.util.validation;

import com.rto.model.entity.Route;
import com.rto.model.entity.Station;
import com.rto.model.entity.Train;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Mykyta Blyzniukov
 */

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RouteValidation {

    private static boolean isDepartureAndDestinationStationSame(Station departure, Station destination) {
        return departure.equals(destination);
    }

    public static boolean isRouteInputIsNotValid(String name,
                                                 String status,
                                                 Station departureStation,
                                                 Station destinationStation,
                                                 Train train,
                                                 Route route) {
        return route == null
                || departureStation == null
                || destinationStation == null
                || train == null
                || StringUtils.isBlank(name) || StringUtils.isBlank(status)
                || RouteValidation.isDepartureAndDestinationStationSame(departureStation, destinationStation);
    }
}
