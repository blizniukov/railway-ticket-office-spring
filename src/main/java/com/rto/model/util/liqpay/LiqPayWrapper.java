package com.rto.model.util.liqpay;

import com.liqpay.LiqPay;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Nikita Blyzniukov
 */
@Component
public class LiqPayWrapper extends LiqPay {

    public LiqPayWrapper(@Value("${spring.liqpay.public-key}") String publicKey,
                         @Value("${spring.liqpay.private-key}") String privateKey) {
        super(publicKey, privateKey);
    }

    @Override
    public String str_to_sign(String str) {
        return super.str_to_sign(str);
    }
}
