package com.rto.model.service;

import com.rto.model.entity.Option;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface OptionService {

    /**
     * Save option in database
     *
     * @param option object to save
     */
    void create(Option option);

    /**
     * Update Option in database
     *
     * @param id     id of Option to update
     * @param option object with new values
     */
    void update(Long id, Option option);

    /**
     * Delete Option from database
     *
     * @param id object to delete
     */
    void delete(Long id);

    /**
     * Get Option from database by Name
     *
     * @param name of option to select
     * @return Option object
     */
    Option getByName(String name);

    /**
     * Get Option from database by ID
     *
     * @param id of option to select
     * @return Option object
     */
    Option getById(Long id);

    /**
     * Get all options from Database
     *
     * @return List of Option
     */
    List<Option> getAll();
}
