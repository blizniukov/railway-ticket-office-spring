package com.rto.model.service;

import com.rto.model.entity.VerifyToken;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface VerifyTokenService {

    /**
     * Save verifyToken in database
     *
     * @param verifyToken object to save
     */
    void create(VerifyToken verifyToken);

    /**
     * Update verifyToken in database
     *
     * @param id          id of verifyToken to update
     * @param verifyToken object with new values
     */
    void update(Long id, VerifyToken verifyToken);

    /**
     * Delete verifyToken from database
     *
     * @param id object to delete
     */
    void delete(Long id);

    /**
     * Delete all given verifytokens
     *
     * @param tokens to delete
     */
    void deleteAll(List<VerifyToken> tokens);

    /**
     * Get verifyToken from database by ID
     *
     * @param id of verifyToken to select
     * @return VerifyToken object
     */
    VerifyToken getById(Long id);

    /**
     * Get verifyToken from database by TOKEN
     *
     * @param token of verifyToken to select
     * @return VerifyToken object
     */
    VerifyToken getByToken(String token);

    /**
     * Get all verifyTokens from Database
     * return List of VerifyTokens
     */
    List<VerifyToken> getAll();

    /**
     * Get all expired tokens
     *
     * @param date of expire
     * @return List of tokens
     */
    List<VerifyToken> getAllExpiresBeforeDate(LocalDate date);
}
