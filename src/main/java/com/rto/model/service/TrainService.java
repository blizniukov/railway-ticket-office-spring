package com.rto.model.service;

import com.rto.model.entity.Train;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */

public interface TrainService {
    /**
     * Save train in database
     *
     * @param train object to save
     */
    void create(Train train);

    /**
     * Update train in database
     *
     * @param id    id of train to update
     * @param train object with new values
     */
    void update(Long id, Train train);

    /**
     * Delete train from database
     *
     * @param id object to delete
     */
    void delete(Long id);

    /**
     * Get all trains from Database
     * return List of Trains
     */
    List<Train> getAll();

    /**
     * Get train from database by ID
     *
     * @param id of train to select
     * @return Train object
     */
    Train getById(Long id);

    /**
     * Get train from database by NAME
     *
     * @param name of train to select
     * @return Train object
     */
    Train getByName(final String name);

    /**
     * Get free trains ready for route from database
     *
     * @return List of Trains
     */
    List<Train> getFreeTrains();
}
