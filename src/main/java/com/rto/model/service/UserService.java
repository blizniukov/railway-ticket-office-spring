package com.rto.model.service;

import com.rto.model.entity.User;
import com.rto.model.entity.VerifyToken;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface UserService {

    /**
     * Save user to database and sendVerifyToken
     *
     * @param user to save
     */
    VerifyToken createAndGenerateVerifyToken(User user);

    /**
     * Update existing User in database
     *
     * @param id   of user to update
     * @param user with new values
     */
    void update(Long id, User user);

    /**
     * Delete user from database
     *
     * @param id of User
     */
    void delete(Long id);

    /**
     * Get User from database by Id
     *
     * @param id of User
     * @return User object
     */
    User getById(Long id);

    /**
     * Get User from database by login
     *
     * @param login of User
     * @return User object
     */
    User getByLogin(String login);

    /**
     * Get User from database by email
     *
     * @param email of User
     * @return User object
     */
    User getByEmail(String email);

    /**
     * Get all Users from database
     *
     * @return List of Users
     */
    List<User> getAll();

    /**
     * Check users login on unique
     *
     * @param login to check
     * @return boolean value
     */
    boolean isUniqueLogin(final String login);

    /**
     * Check users email on unique
     *
     * @param email to check
     * @return boolean value
     */
    boolean isEmailInUse(final String email);

    /**
     * Confirm user registration and delete VerifyToken after confirmation
     *
     * @param user        to confirm registration
     * @param verifyToken to delete
     */
    void confirmUserAndDeleteToken(User user, VerifyToken verifyToken);
}
