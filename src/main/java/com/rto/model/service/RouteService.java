package com.rto.model.service;

import com.rto.model.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface RouteService {

    /**
     * Save Route in database
     *
     * @param route object to save
     */
    void create(Route route);

    /**
     * Update Route in database
     *
     * @param id    id of Route to update
     * @param route object with new values
     */
    void update(Long id, Route route);

    /**
     * Delete Route from database
     *
     * @param id object to delete
     */
    void delete(Long id);

    /**
     * Update given routes
     *
     * @param routes to update
     */
    void updateAll(List<Route> routes);

    /**
     * Get Route from database by ID
     *
     * @param id of Route to select
     * @return Route object
     */
    Route getById(Long id);

    /**
     * Get Route from database by NAME
     *
     * @param name of Route to select
     * @return Route object
     */
    Route getByName(String name);

    /**
     * Get all Routes
     *
     * @return List of Routes
     */
    List<Route> getAll();

    /**
     * Search Route for User request
     *
     * @param departure     Station
     * @param destination   Station
     * @param departureDate of route
     * @param endDay        util param to plus 23 hours
     * @param status        of Route
     * @param pageable      object for pages
     * @return Route Page
     */
    Page<Route> searchRoute(Station departure,
                            Station destination,
                            LocalDateTime departureDate,
                            LocalDateTime endDay,
                            RouteStatus status,
                            Pageable pageable);

    /**
     * Find all routes which status are not given
     *
     * @param status   to exclude
     * @param pageable object
     * @return Page with routes
     */
    Page<Route> findRoutesAfterDate(LocalDateTime ldt, RouteStatus status, Pageable pageable);

    /**
     * Check is route active and not expired
     *
     * @param route to check
     * @return true if yes and false if not
     */
    boolean isRouteActive(Route route);

    /**
     * Get all Routes which are already finished before date
     *
     * @param dateTime to fetch Routes
     * @return List of Route
     */
    List<Route> getAllFinishedRoutesBeforeDate(LocalDateTime dateTime);

    /**
     * Check is Route valid and Seat status is FREE
     *
     * @param route to check
     * @param seat  to check
     * @return true if seat is free
     */
    boolean isRouteValidAndSeatFree(Route route, Seat seat);
}
