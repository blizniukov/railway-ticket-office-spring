package com.rto.model.service.implementation;

import com.rto.model.entity.Train;
import com.rto.model.repository.TrainRepository;
import com.rto.model.service.TrainService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class TrainServiceImpl implements TrainService {

    private final TrainRepository trainRepository;

    @Override
    public void create(Train train) {
        trainRepository.save(train);
    }

    @Override
    @Transactional
    public void update(Long id, Train train) {
        Train trainToUpdate = trainRepository.getById(id);
        trainToUpdate.setName(train.getName());
        trainToUpdate.setType(train.getType());
        trainToUpdate.setWagons(train.getWagons());
        trainRepository.save(train);
    }

    @Override
    public void delete(Long id) {
        trainRepository.deleteById(id);
    }

    @Override
    public List<Train> getAll() {
        return trainRepository.findAll();
    }

    @Override
    public Train getById(Long id) {
        return trainRepository.getById(id);
    }

    @Override
    public Train getByName(String name) {
        return trainRepository.getByName(name);
    }

    @Override
    public List<Train> getFreeTrains() {
        return trainRepository.getAllByRouteNull();
    }
}
