package com.rto.model.service.implementation;

import com.rto.model.entity.RoleName;
import com.rto.model.entity.User;
import com.rto.model.entity.VerifyToken;
import com.rto.model.repository.UserRepository;
import com.rto.model.service.RoleService;
import com.rto.model.service.UserService;
import com.rto.model.service.VerifyTokenService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final VerifyTokenService verifyTokenService;

    @Override
    @Transactional
    public VerifyToken createAndGenerateVerifyToken(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(roleService.getByName(RoleName.ROLE_USER));
        userRepository.save(user);

        VerifyToken verifyToken = new VerifyToken(user);
        verifyTokenService.create(verifyToken);
        return verifyToken;
    }


    @Transactional
    @Override
    public void confirmUserAndDeleteToken(User user, VerifyToken verifyToken) {
        user.setActive(true);
        update(user.getId(), user);
        delete(verifyToken.getId());
    }

    @Override
    public void update(Long id, User user) {
        User userToUpdate = userRepository.getById(id);
        userToUpdate.setLogin(user.getLogin());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setActive(user.isActive());
        userToUpdate.setRole(user.getRole());
        userToUpdate.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User getById(Long id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByLogin(String login) {
        return userRepository.getByLogin(login);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public boolean isUniqueLogin(String login) {
        return !Optional.ofNullable(getByLogin(login)).isPresent();
    }

    @Override
    public boolean isEmailInUse(String email) {
        return Optional.ofNullable(getByEmail(email)).isPresent();
    }
}
