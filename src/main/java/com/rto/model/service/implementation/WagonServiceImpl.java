package com.rto.model.service.implementation;

import com.rto.model.dto.TrainCountSeatsDto;
import com.rto.model.dto.TrainTypeCountDto;
import com.rto.model.entity.Train;
import com.rto.model.entity.Wagon;
import com.rto.model.entity.WagonType;
import com.rto.model.repository.WagonRepository;
import com.rto.model.service.WagonService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class WagonServiceImpl implements WagonService {

    private final WagonRepository wagonRepository;

    @Override
    public void create(Wagon wagon) {
        wagonRepository.save(wagon);
    }

    @Override
    public void update(Long id, Wagon wagon) {
        Wagon wagonToUpdate = wagonRepository.getById(id);
        wagonToUpdate.setNumber(wagon.getNumber());
        wagonToUpdate.setCountOfSeats(wagon.getCountOfSeats());
        wagonToUpdate.setSeatCost(wagon.getSeatCost());
        wagonToUpdate.setType(wagon.getType());
        wagonToUpdate.setSeats(wagon.getSeats());
        wagonToUpdate.setTrain(wagon.getTrain());
        wagonRepository.save(wagonToUpdate);
    }

    @Override
    public void delete(Long id) {
        wagonRepository.deleteById(id);
    }

    @Override
    public Wagon getById(Long id) {
        return wagonRepository.getById(id);
    }

    @Override
    public List<Wagon> getAll() {
        return wagonRepository.findAll();
    }

    @Override
    public List<Wagon> getTrainWagonsOfType(Train train, WagonType type) {
        return wagonRepository.getWagonsByTrainAndType(train, type);
    }

    @Override
    public Long getMinCostOfSeatByWagonType(Train train, WagonType type) {
        return getTrainWagonsOfType(train, type)
                .stream()
                .mapToLong(Wagon::getSeatCost)
                .min()
                .orElse(0L);
    }

    @Override
    public Long getCountOfSeatsOfTrainByWagonType(final Train train, final WagonType type) {
        return getTrainWagonsOfType(train, type)
                .stream()
                .mapToLong(Wagon::getCountOfSeats)
                .sum();
    }

    @Override
    public TrainTypeCountDto getTrainMinCostAndCount(final Train train, final WagonType type) {
        Long minCostOfSeatByWagonType = getMinCostOfSeatByWagonType(train, type);
        Long countOfSeatsOfTrainByWagonType = getCountOfSeatsOfTrainByWagonType(train, type);
        return new TrainTypeCountDto(type.toString(), minCostOfSeatByWagonType, countOfSeatsOfTrainByWagonType);
    }

    @Override
    public TrainCountSeatsDto getWagonsAttributes(Train train) {
        TrainTypeCountDto economy = getTrainMinCostAndCount(train, WagonType.ECONOMY);
        TrainTypeCountDto business = getTrainMinCostAndCount(train, WagonType.BUSINESS);
        TrainTypeCountDto standard = getTrainMinCostAndCount(train, WagonType.STANDARD);

        return TrainCountSeatsDto.builder()
                .trainName(train.getName())
                .types(Arrays.asList(business, economy, standard))
                .build();
    }
}
