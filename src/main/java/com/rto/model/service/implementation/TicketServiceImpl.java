package com.rto.model.service.implementation;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.rto.model.entity.*;
import com.rto.model.repository.TicketRepository;
import com.rto.model.service.SeatService;
import com.rto.model.service.TicketService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;
    private final SeatService seatService;

    @Override
    public void create(Ticket ticket) {
        ticket.setPayId(generateUniquePayId());
        ticketRepository.save(ticket);
    }

    @Override
    @Transactional
    public void update(Long id, Ticket ticket) {
        Ticket ticketToUpdate = ticketRepository.getById(id);
        ticketToUpdate.setFirstName(ticket.getFirstName());
        ticketToUpdate.setLastName(ticket.getLastName());
        ticketToUpdate.setCost(ticket.getCost());
        ticketToUpdate.setRoute(ticket.getRoute());
        ticketToUpdate.setPayId(ticket.getPayId());
        ticketToUpdate.setTicketStatus(ticket.getTicketStatus());
        ticketToUpdate.setSeat(ticket.getSeat());
        ticketToUpdate.setWagon(ticket.getWagon());
        ticketToUpdate.setUser(ticket.getUser());
        ticketRepository.save(ticketToUpdate);
    }

    @Override
    public void updateAll(List<Ticket> tickets) {
        ticketRepository.saveAll(tickets);
    }

    @Override
    public void delete(Long id) {
        ticketRepository.deleteById(id);
    }

    @Override
    public Ticket getById(Long id) {
        return ticketRepository.getById(id);
    }

    @Override
    public List<Ticket> getByUser(User user) {
        return ticketRepository.getTicketsByUser(user);
    }

    @Override
    public List<Ticket> getAll() {
        return ticketRepository.findAll();
    }

    @Override
    public Ticket getByPayId(String payId) {
        return ticketRepository.getByPayId(payId);
    }

    @Override
    @Transactional
    public void createTicketAndMakeSeatPending(Ticket ticket, Seat seat) {
        seat.setStatus(SeatStatus.PENDING);
        seatService.update(seat.getId(), seat);
        create(ticket);
    }

    @Override
    @Transactional
    public void bookTicketForSeat(Ticket ticket) {
        Seat seat = ticket.getSeat();
        ticket.setTicketStatus(TicketStatus.ACTIVE);
        seat.setStatus(SeatStatus.BOOKED);
        update(ticket.getId(), ticket);
        seatService.update(seat.getId(), seat);
    }

    @Override
    @Transactional
    public void returnTicketAndMakePlaceFree(Ticket ticket) {

        ticket.setTicketStatus(TicketStatus.RETURNED);
        update(ticket.getId(), ticket);
        Seat seat = ticket.getSeat();
        seat.setStatus(SeatStatus.FREE);
        seatService.update(seat.getId(), seat);
    }

    @Override
    @SneakyThrows
    public String generateQrCodeByPayId(String payId) {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix qr = qrCodeWriter.encode(payId, BarcodeFormat.QR_CODE, 200, 200);

        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(qr, "PNG", pngOutputStream);
        byte[] pngData = pngOutputStream.toByteArray();
        return Base64.getEncoder().encodeToString(pngData);
    }

    private String generateUniquePayId() {
        String data = "unique_pay_id_" + UUID.randomUUID().toString();
        return DatatypeConverter.printBase64Binary(data.getBytes());
    }
}
