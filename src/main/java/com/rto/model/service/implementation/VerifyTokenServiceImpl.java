package com.rto.model.service.implementation;

import com.rto.model.entity.VerifyToken;
import com.rto.model.repository.VerifyTokenRepository;
import com.rto.model.service.VerifyTokenService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class VerifyTokenServiceImpl implements VerifyTokenService {

    private final VerifyTokenRepository verifyTokenRepository;

    @Override
    public void create(VerifyToken verifyToken) {
        verifyTokenRepository.save(verifyToken);
    }

    @Override
    public void update(Long id, VerifyToken verifyToken) {
        VerifyToken tokenToUpdate = verifyTokenRepository.getById(id);
        tokenToUpdate.setExpiryDate(verifyToken.getExpiryDate());
        tokenToUpdate.setToken(verifyToken.getToken());
        tokenToUpdate.setUser(verifyToken.getUser());
        verifyTokenRepository.save(tokenToUpdate);
    }

    @Override
    public void delete(Long id) {
        verifyTokenRepository.deleteById(id);
    }

    @Override
    public void deleteAll(List<VerifyToken> tokens) {
        verifyTokenRepository.deleteAll(tokens);
    }

    @Override
    public VerifyToken getById(Long id) {
        return verifyTokenRepository.getById(id);
    }

    @Override
    public VerifyToken getByToken(String token) {
        return verifyTokenRepository.getByToken(token);
    }

    @Override
    public List<VerifyToken> getAll() {
        return verifyTokenRepository.findAll();
    }

    @Override
    public List<VerifyToken> getAllExpiresBeforeDate(LocalDate date) {
        return verifyTokenRepository.findVerifyTokensByExpiryDateIsBefore(date);
    }
}
