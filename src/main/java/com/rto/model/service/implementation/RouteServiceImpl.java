package com.rto.model.service.implementation;

import com.rto.model.entity.*;
import com.rto.model.repository.RouteRepository;
import com.rto.model.service.RouteService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class RouteServiceImpl implements RouteService {

    private final RouteRepository routeRepository;

    @Override
    public void create(Route route) {
        routeRepository.save(route);
    }

    @Override
    public void update(Long id, Route route) {
        Route routeToUpdate = routeRepository.getById(id);
        routeToUpdate.setName(route.getName());
        routeToUpdate.setDeparture(route.getDeparture());
        routeToUpdate.setDestination(route.getDestination());
        routeToUpdate.setDepartureDate(route.getDepartureDate());
        routeToUpdate.setArrivalDate(route.getArrivalDate());
        routeToUpdate.setTrain(route.getTrain());
        routeToUpdate.setRouteStatus(route.getRouteStatus());
        routeRepository.save(routeToUpdate);
    }

    @Override
    public void delete(Long id) {
        routeRepository.deleteById(id);
    }

    @Override
    public void updateAll(List<Route> routes) {
        routeRepository.saveAll(routes);
    }

    @Override
    public Route getById(Long id) {
        return routeRepository.getById(id);
    }

    @Override
    public Route getByName(String name) {
        return routeRepository.getByName(name);
    }

    @Override
    public List<Route> getAll() {
        return routeRepository.findAll();
    }

    @Override
    public Page<Route> searchRoute(Station departure,
                                   Station destination,
                                   LocalDateTime departureDate,
                                   LocalDateTime endDay,
                                   RouteStatus status,
                                   Pageable pageable) {
        return routeRepository.searchRouteForRequest(departure,
                destination,
                departureDate,
                endDay,
                status,
                pageable);
    }

    @Override
    public boolean isRouteActive(Route route) {
        return route.getDepartureDate().isAfter(LocalDateTime.now());
    }

    @Override
    public Page<Route> findRoutesAfterDate(LocalDateTime ldt, RouteStatus status, Pageable pageable) {
        return routeRepository.findRoutesByDepartureDateIsAfterAndRouteStatusIsNot(ldt, status, pageable);
    }

    @Override
    public List<Route> getAllFinishedRoutesBeforeDate(LocalDateTime dateTime) {
        return routeRepository.findRoutesByArrivalDateIsBefore(dateTime);
    }

    @Override
    public boolean isRouteValidAndSeatFree(Route route, Seat seat) {
        return isRouteActive(route) && seat.getStatus().toString().equals("FREE");
    }
}
