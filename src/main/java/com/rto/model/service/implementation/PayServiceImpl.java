package com.rto.model.service.implementation;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rto.model.dto.ResponseLiqPay;
import com.rto.model.entity.Ticket;
import com.rto.model.service.PayService;
import com.rto.model.util.liqpay.LiqPayWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nikita Blyzniukov
 */
@Service
@Log4j
@RequiredArgsConstructor
public class PayServiceImpl implements PayService {

    @Value("${spring.liqpay.public-key}")
    private String publicKey;
    @Value("${spring.liqpay.private-key}")
    private String privateKey;

    private final LiqPayWrapper liqPayWrapper;

    @Override
    public Map<String, String> configurePayRequest(Ticket ticket) {
        Map<String, String> params = new HashMap<>();
        params.put("amount", ticket.getCost().toString());
        params.put("currency", "UAH");
        params.put("description", "Pay of Fine for Route: " + ticket.getRoute().getName() +
                " Wagon: " + ticket.getWagon().getNumber() +
                " Seat: " + ticket.getSeat().getSeatNumber());
        params.put("sandbox", "1");
        params.put("server_url", "/tickets/" + ticket.getId() + "/pay");
        params.put("order_id", ticket.getPayId());
        params.put("action", "pay");
        params.put("email", ticket.getUser().getEmail());
        params.put("result_url", "localhost:8080/tickets/checkPayForTicket");
        return params;
    }

    @Override
    public String encodeResponseToBase64(String data) {
        return liqPayWrapper.str_to_sign(privateKey + data + privateKey);
    }

    @Override
    public ResponseLiqPay deserializeResponse(String decoded) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return mapper.readValue(decoded, ResponseLiqPay.class);
    }

    @Override
    public boolean isPaySuccess(String status) {
        return status.equalsIgnoreCase("sandbox")
                || status.equalsIgnoreCase("success");
    }
}
