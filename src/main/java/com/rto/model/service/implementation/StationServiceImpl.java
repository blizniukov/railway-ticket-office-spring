package com.rto.model.service.implementation;

import com.rto.model.entity.Station;
import com.rto.model.repository.StationRepository;
import com.rto.model.service.StationService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class StationServiceImpl implements StationService {

    private final StationRepository stationRepository;

    @Override
    public void create(Station station) {
        stationRepository.save(station);
    }

    @Override
    public void update(Long id, Station station) {
        Station stationToUpdate = stationRepository.getById(id);
        stationToUpdate.setName(station.getName());
        stationRepository.save(stationToUpdate);
    }

    @Override
    public void delete(Long id) {
        stationRepository.deleteById(id);
    }

    @Override
    public Station getByName(String name) {
        return stationRepository.getByName(name);
    }

    @Override
    public Station getById(Long id) {
        return stationRepository.getById(id);
    }

    @Override
    public Page<Station> getPage(Pageable pageable) {
        return stationRepository.findAll(pageable);
    }

    @Override
    public List<Station> getAll() {
        return stationRepository.findAll();
    }
}
