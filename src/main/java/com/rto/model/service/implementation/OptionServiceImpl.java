package com.rto.model.service.implementation;

import com.rto.model.entity.Option;
import com.rto.model.repository.OptionRepository;
import com.rto.model.service.OptionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class OptionServiceImpl implements OptionService {

    private final OptionRepository optionRepository;

    @Override
    public void create(Option option) {
        optionRepository.save(option);
    }

    @Override
    public void update(Long id, Option option) {
        Option optionToUpdate = getById(id);
        optionToUpdate.setName(option.getName());
        optionToUpdate.setPrice(option.getPrice());
        optionRepository.save(optionToUpdate);
    }

    @Override
    public void delete(Long id) {
        optionRepository.deleteById(id);
    }

    @Override
    public Option getByName(String name) {
        return optionRepository.getByName(name);
    }

    @Override
    public Option getById(Long id) {
        return optionRepository.getById(id);
    }

    @Override
    public List<Option> getAll() {
        return optionRepository.findAll();
    }
}
