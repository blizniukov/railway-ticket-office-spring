package com.rto.model.service.implementation;

import com.rto.model.entity.Seat;
import com.rto.model.entity.SeatStatus;
import com.rto.model.repository.SeatRepository;
import com.rto.model.service.SeatService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
@AllArgsConstructor
public class SeatServiceImpl implements SeatService {

    private final SeatRepository seatRepository;

    @Override
    public void create(Seat seat) {
        seatRepository.save(seat);
    }

    @Override
    @Transactional
    public void update(Long id, Seat seat) {
        Seat seatToUpdate = seatRepository.getById(id);
        seatToUpdate.setSeatNumber(seat.getSeatNumber());
        seatToUpdate.setStatus(seat.getStatus());
        seatRepository.save(seatToUpdate);
    }

    @Override
    @Transactional
    public void updateAll(List<Seat> seats) {
        seatRepository.saveAll(seats);
    }

    @Override
    public void delete(Long id) {
        seatRepository.deleteById(id);
    }

    @Override
    public Seat getById(Long id) {
        return seatRepository.getById(id);
    }

    @Override
    public List<Seat> getAll() {
        return seatRepository.findAll();
    }

    @Override
    public List<Seat> getAllByStatus(SeatStatus status) {
        return seatRepository.getSeatsByStatus(status);
    }
}
