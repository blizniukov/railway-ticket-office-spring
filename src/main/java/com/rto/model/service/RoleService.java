package com.rto.model.service;

import com.rto.model.entity.Role;
import com.rto.model.entity.RoleName;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface RoleService {

    /**
     * Save Role in database
     *
     * @param role to save
     */
    void create(Role role);

    /**
     * Get Role from database by id
     *
     * @param id of Role to find
     * @return Role object
     */
    Role getById(Long id);

    /**
     * Get Role from database by RoleName
     *
     * @param name of Role to find
     * @return Role object
     */
    Role getByName(RoleName name);

    /**
     * Get all Roles from database
     *
     * @return List of Roles
     */
    List<Role> getAll();
}
