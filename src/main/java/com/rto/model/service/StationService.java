package com.rto.model.service;

import com.rto.model.entity.Station;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface StationService {

    /**
     * Save Station in database
     *
     * @param station object to save
     */
    void create(Station station);

    /**
     * Update Station in database
     *
     * @param id      id of Station to update
     * @param station object with new values
     */
    void update(Long id, Station station);

    /**
     * Delete Station from database
     *
     * @param id object to delete
     */
    void delete(Long id);

    /**
     * Get Station from database by Name
     *
     * @param name of Station to select
     * @return Station object
     */
    Station getByName(String name);

    /**
     * Get Station from database by ID
     *
     * @param id of Station to select
     * @return Station object
     */
    Station getById(Long id);

    /**
     * Get page with Stations Stations
     *
     * @return Page of Stations
     */
    Page<Station> getPage(Pageable pageable);

    /**
     * Get all Stations
     *
     * @return List of Stations
     */
    List<Station> getAll();
}
