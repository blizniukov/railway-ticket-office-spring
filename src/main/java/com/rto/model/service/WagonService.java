package com.rto.model.service;

import com.rto.model.dto.TrainTypeCountDto;
import com.rto.model.entity.Train;
import com.rto.model.entity.Wagon;
import com.rto.model.entity.WagonType;
import com.rto.model.dto.TrainCountSeatsDto;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */

public interface WagonService {

    /**
     * Save Wagon in database
     *
     * @param wagon object to save
     */
    void create(Wagon wagon);

    /**
     * Update Wagon in database
     *
     * @param id    id of Wagon to update
     * @param wagon object with new values
     */
    void update(Long id, Wagon wagon);

    /**
     * Delete Wagon from database
     *
     * @param id object to delete
     */
    void delete(Long id);

    /**
     * Get Wagon from database by ID
     *
     * @param id of Wagon to select
     * @return Wagon object
     */
    Wagon getById(Long id);

    /**
     * Get all Wagons
     *
     * @return List of Wagons
     */
    List<Wagon> getAll();

    /**
     * Get wagons of train of required type
     *
     * @param train to get wagons
     * @param type  of wagons
     */
    List<Wagon> getTrainWagonsOfType(Train train, WagonType type);

    /**
     * Get total count of seats in train
     *
     * @param train to get count of seats
     * @param type  of wagons
     */
    Long getCountOfSeatsOfTrainByWagonType(Train train, WagonType type);

    /**
     * Get train count of seats
     *
     * @param train to fetch data
     * @return Data transfer object with values
     */
    TrainCountSeatsDto getWagonsAttributes(Train train);

    /**
     * Get min cost of seat of all wagons by single type in train
     *
     * @param train to get min cost
     * @param type  wagon type to search
     * @return Long number
     */
    Long getMinCostOfSeatByWagonType(Train train, WagonType type);

    /**
     * Get Train statistics by Wagon type.
     * Get count of all seats of input type, return min seat cost
     *
     * @param train to get seats and min cost count
     * @param type  to get seats and min cost count
     * @return TrainTypeCountDto object
     */
    TrainTypeCountDto getTrainMinCostAndCount(Train train, WagonType type);
}
