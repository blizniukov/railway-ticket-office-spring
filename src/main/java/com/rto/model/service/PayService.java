package com.rto.model.service;

import com.rto.model.dto.ResponseLiqPay;
import com.rto.model.entity.Ticket;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface PayService {

    /**
     * Configure pay request for given ticket
     *
     * @param ticket to configure pay request
     * @return Map with settings
     */
    Map<String, String> configurePayRequest(Ticket ticket);

    /**
     * Encode data response to base64
     *
     * @param data to encode
     * @return sign
     */
    String encodeResponseToBase64(String data);

    /**
     * Get ResponseLiqPay object from decoded String
     *
     * @param decoded string to parse object
     * @return ResponseLiqPay object
     */
    ResponseLiqPay deserializeResponse(String decoded) throws IOException;

    /**
     * Validate is pay was successful
     *
     * @param status of pay
     * @return boolean true if yes or false if not
     */
    boolean isPaySuccess(String status);
}
