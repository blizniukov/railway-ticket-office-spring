package com.rto.model.service;

import com.rto.model.entity.Seat;
import com.rto.model.entity.Ticket;
import com.rto.model.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface TicketService {

    /**
     * Save Ticket in database
     *
     * @param ticket object to save
     */
    void create(Ticket ticket);

    /**
     * Update Ticket in database
     *
     * @param id     id of Ticket to update
     * @param ticket object with new values
     */
    void update(Long id, Ticket ticket);

    /**
     * Update given tickets
     *
     * @param tickets to update
     */
    void updateAll(List<Ticket> tickets);

    /**
     * Delete Ticket from database
     *
     * @param id object to delete
     */
    void delete(Long id);

    /**
     * Get Ticket from database by ID
     *
     * @param id of Ticket to select
     * @return Ticket object
     */
    Ticket getById(Long id);

    /**
     * Get Tickets from database by USER
     *
     * @param user of Ticket to select
     * @return List of tickets
     */
    List<Ticket> getByUser(User user);

    /**
     * Get all Tickets
     *
     * @return List of Tickets
     */
    List<Ticket> getAll();

    /**
     * Get ticket by pay id
     *
     * @param payId to search ticket
     * @return Ticket object
     */
    Ticket getByPayId(String payId);

    /**
     * Create new Ticket with Pending status and change
     * status of Seat to pending for 15 mins
     *
     * @param ticket to create
     * @param seat   to change state
     */
    void createTicketAndMakeSeatPending(Ticket ticket, Seat seat);

    /**
     * Book ticket for a seat in database
     *
     * @param ticket to update
     */
    void bookTicketForSeat(Ticket ticket);

    /**
     * Return Ticket and change Seat status to FREE
     *
     * @param ticket to return
     */
    void returnTicketAndMakePlaceFree(Ticket ticket);

    /**
     * Generate QR code for ticket by payId
     *
     * @param payId to generate QR code
     * @return encoded to BASE64 String
     */
    String generateQrCodeByPayId(String payId);
}
