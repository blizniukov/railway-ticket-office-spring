package com.rto.model.service;

import com.rto.model.entity.Seat;
import com.rto.model.entity.SeatStatus;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Service
public interface SeatService {

    /**
     * Save Seat in database
     *
     * @param seat object to save
     */
    void create(Seat seat);

    /**
     * Update Seat in database
     *
     * @param id   id of Seat to update
     * @param seat object with new values
     */
    void update(Long id, Seat seat);

    /**
     * Update given seats
     *
     * @param seats to update
     */
    void updateAll(List<Seat> seats);

    /**
     * Delete Seat from database
     *
     * @param id object to delete
     */
    void delete(Long id);

    /**
     * Get Seat from database by ID
     *
     * @param id of Seat to select
     * @return Seat object
     */
    Seat getById(Long id);

    /**
     * Get all Seats
     *
     * @return List of Seats
     */
    List<Seat> getAll();

    /**
     * Get all Seats of given status
     *
     * @param status to fetch seats
     * @return List of Seats
     */
    List<Seat> getAllByStatus(SeatStatus status);
}
