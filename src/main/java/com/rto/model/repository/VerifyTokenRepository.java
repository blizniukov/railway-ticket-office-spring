package com.rto.model.repository;

import com.rto.model.entity.VerifyToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface VerifyTokenRepository extends JpaRepository<VerifyToken, Long> {

    VerifyToken getById(Long id);

    VerifyToken getByToken(String token);

    List<VerifyToken> findVerifyTokensByExpiryDateIsBefore(LocalDate date);
}
