package com.rto.model.repository;

import com.rto.model.entity.Station;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface StationRepository extends PagingAndSortingRepository<Station, Long> {

    Station getById(Long id);

    Station getByName(String name);

    List<Station> findAll();
}
