package com.rto.model.repository;

import com.rto.model.entity.Train;
import com.rto.model.entity.Wagon;
import com.rto.model.entity.WagonType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface WagonRepository extends JpaRepository<Wagon, Long> {

    Wagon getById(Long id);

    List<Wagon> getWagonsByTrainAndType(Train train, WagonType type);
}
