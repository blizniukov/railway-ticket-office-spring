package com.rto.model.repository;

import com.rto.model.entity.Train;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface TrainRepository extends JpaRepository<Train, Long> {

    Train getById(Long id);

    Train getByName(String name);

    List<Train> getAllByRouteNull();
}
