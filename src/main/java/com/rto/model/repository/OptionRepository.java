package com.rto.model.repository;

import com.rto.model.entity.Option;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface OptionRepository extends JpaRepository<Option, Long> {

    Option getById(Long id);

    Option getByName(String name);
}
