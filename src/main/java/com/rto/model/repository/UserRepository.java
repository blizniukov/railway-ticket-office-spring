package com.rto.model.repository;

import com.rto.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User getById(Long id);

    User getByLogin(String login);

    User getByEmail(String email);
}
