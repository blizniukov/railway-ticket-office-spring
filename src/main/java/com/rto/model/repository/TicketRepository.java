package com.rto.model.repository;

import com.rto.model.entity.Ticket;
import com.rto.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Ticket getById(Long id);

    List<Ticket> getTicketsByUser(User user);

    Ticket getByPayId(String payId);
}
