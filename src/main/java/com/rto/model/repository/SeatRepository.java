package com.rto.model.repository;

import com.rto.model.entity.Seat;
import com.rto.model.entity.SeatStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface SeatRepository extends JpaRepository<Seat, Long> {

    Seat getById(Long id);

    List<Seat> getSeatsByStatus(SeatStatus status);
}
