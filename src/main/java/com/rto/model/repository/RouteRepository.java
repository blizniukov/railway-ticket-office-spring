package com.rto.model.repository;

import com.rto.model.entity.Route;
import com.rto.model.entity.RouteStatus;
import com.rto.model.entity.Station;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface RouteRepository extends PagingAndSortingRepository<Route, Long> {

    Route getById(Long id);

    Route getByName(String name);

    List<Route> findAll();

    Page<Route> findRoutesByDepartureDateIsAfterAndRouteStatusIsNot(LocalDateTime ldt, RouteStatus status, Pageable pageable);

    @Query(value = "select r from Route r where r.departure = :departure " +
            "and r.destination = :destination and r.routeStatus not like :status and r.departureDate between :date and :endDay")
    Page<Route> searchRouteForRequest(@Param("departure") Station departure,
                                      @Param("destination") Station destination,
                                      @Param("date") LocalDateTime departureDate,
                                      @Param("endDay") LocalDateTime endDay,
                                      @Param("status") RouteStatus status,
                                      Pageable pageable);

    List<Route> findRoutesByArrivalDateIsBefore(LocalDateTime dateTime);
}
