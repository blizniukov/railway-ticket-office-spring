package com.rto.model.repository;

import com.rto.model.entity.Role;
import com.rto.model.entity.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nikita Blyzniukov
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role getById(Long id);

    Role findByName(RoleName roleName);
}
