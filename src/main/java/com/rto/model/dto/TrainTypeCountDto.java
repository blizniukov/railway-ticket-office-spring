package com.rto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Nikita Blyzniukov
 */

@Data
@AllArgsConstructor
public class TrainTypeCountDto {
    private String name;
    private Long cost;
    private Long count;
}

