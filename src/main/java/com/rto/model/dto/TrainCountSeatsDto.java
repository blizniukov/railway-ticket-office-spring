package com.rto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TrainCountSeatsDto {
    private String trainName;
    private List<TrainTypeCountDto> types;
}
