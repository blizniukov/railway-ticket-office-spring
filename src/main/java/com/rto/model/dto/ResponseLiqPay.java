package com.rto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Nikita Blyzniukov
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseLiqPay {
    private String status;
    private String order_id;
}
