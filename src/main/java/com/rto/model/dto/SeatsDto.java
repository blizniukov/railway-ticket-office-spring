package com.rto.model.dto;

import com.rto.model.entity.Seat;
import lombok.Data;

import java.util.List;

/**
 * @author Nikita Blyzniukov
 */

@Data
public class SeatsDto {
    private List<Seat> seats;
}
