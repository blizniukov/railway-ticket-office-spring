package com.rto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Nikita Blyzniukov
 */
@AllArgsConstructor
@Data
public class ApiResponse {
    private boolean success;
    private String message;
}
