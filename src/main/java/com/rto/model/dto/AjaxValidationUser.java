package com.rto.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Nikita Blyzniukov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AjaxValidationUser {
    String login;
    String password;
}
