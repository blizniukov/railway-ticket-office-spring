package com.rto.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.UUID;

/**
 * @author Nikita Blyzniukov
 */

@Data
@Entity
@NoArgsConstructor
@Table(name = "verifytokens")
public class VerifyToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String token;

    @OneToOne(targetEntity = User.class)
    @JoinColumn(nullable = false, name = "userId")
    private User user;

    private LocalDate expiryDate;

    public VerifyToken(User user) {
        this.user = user;
        this.token = UUID.randomUUID().toString();
        this.expiryDate = LocalDate.now();
    }
}
