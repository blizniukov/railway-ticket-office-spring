package com.rto.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"wagons", "route"})
@Entity
@Table(name = "trains")
public class Train implements Serializable {
    private static final long serialVersionUID = -2595900592145305827L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @OneToOne(mappedBy = "train")
    private Route route;

    @Enumerated(EnumType.STRING)
    private TrainType type;

    @OneToMany(mappedBy = "train")
    @JsonIgnore
    private List<Wagon> wagons;

    public Train(final String name, final TrainType type) {
        this.name = name;
        this.type = type;
    }
}
