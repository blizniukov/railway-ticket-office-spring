package com.rto.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Nikita Blyzniukov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "routes")
public class Route implements Serializable {
    private static final long serialVersionUID = -8892816860000522740L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;
    private LocalDateTime departureDate;
    private LocalDateTime arrivalDate;

    @OneToOne
    @JoinColumn(name = "departureId", nullable = false)
    private Station departure;

    @OneToOne
    @JoinColumn(name = "destinationId", nullable = false)
    private Station destination;

    @Enumerated(EnumType.STRING)
    private RouteStatus routeStatus;

    @OneToOne(optional = false)
    @JoinColumn(name = "trainId")
    @JsonIgnore
    private Train train;

    public Route(final String name,
                 final LocalDateTime departureDate,
                 final LocalDateTime arrivalDate,
                 final Station departure,
                 final Station destination,
                 final RouteStatus routeStatus,
                 final Train train) {
        this.name = name;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.departure = departure;
        this.destination = destination;
        this.routeStatus = routeStatus;
        this.train = train;
    }
}
