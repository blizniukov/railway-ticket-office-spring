package com.rto.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Nikita Blyzniukov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "seats")
public class Seat implements Serializable {
    private static final long serialVersionUID = -8668547668672103163L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long seatNumber;

    @OneToOne(mappedBy = "seat")
    @JsonIgnore
    private Ticket ticket;

    @Enumerated(EnumType.STRING)
    private SeatStatus status;

    @ManyToOne
    @JoinColumn(name = "wagonId")
    private Wagon wagon;
}
