package com.rto.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Nikita Blyzniukov
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "stations")
public class Station implements Serializable {
    private static final long serialVersionUID = 8828711501625121261L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    public Station(final String name) {
        this.name = name;
    }
}
