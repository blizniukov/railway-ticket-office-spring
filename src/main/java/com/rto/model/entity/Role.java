package com.rto.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString(exclude = "users")
@Builder
@Table(name = "roles")
public class Role implements Serializable {
    private static final long serialVersionUID = 1432374474019050740L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private RoleName name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
    private List<User> users;
}
