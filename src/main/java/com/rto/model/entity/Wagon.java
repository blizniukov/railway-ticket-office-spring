package com.rto.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "seats")
@Builder
@Entity
@Table(name = "wagons")
public class Wagon implements Serializable {
    private static final long serialVersionUID = 7696943529507758704L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long number;

    @Enumerated(EnumType.STRING)
    private WagonType type;

    private Long countOfSeats;
    private Long seatCost;

    @ManyToOne
    @JoinColumn(name = "trainId")
    private Train train;

    @OneToMany(mappedBy = "wagon")
    @JsonIgnore
    private List<Seat> seats;

    public Wagon(final Long number,
                 final WagonType type,
                 final Long countOfSeats,
                 final Long seatCost) {
        this.number = number;
        this.type = type;
        this.countOfSeats = countOfSeats;
        this.seatCost = seatCost;
    }
}
