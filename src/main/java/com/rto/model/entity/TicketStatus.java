package com.rto.model.entity;

public enum TicketStatus {
    PENDING,
    ACTIVE,
    RETURNED,
    EXPIRED
}
