package com.rto.model.entity;

public enum SeatStatus {
    FREE,
    PENDING,
    BOOKED
}
