package com.rto.model.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author Nikita Blyzniukov
 */
@Entity

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "tickets")
@Builder
@Table(name = "users")
public class User implements Serializable {
    private static final long serialVersionUID = -6606681859915610332L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Email
    private String email;

    @NotBlank
    private String login;

    @NotBlank
    private String password;

    private boolean active;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "roleId")
    private Role role;

    @OneToMany(mappedBy = "user")
    private List<Ticket> tickets;
}
