package com.rto.model.entity;

/**
 * @author Nikita Blyzniukov
 */

public enum  WagonType {
    ECONOMY,
    STANDARD,
    BUSINESS
}
