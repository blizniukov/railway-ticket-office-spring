package com.rto.model.entity;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER
}
