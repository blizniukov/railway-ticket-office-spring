package com.rto.model.entity;

public enum TrainType {
    DEFAULT,
    FAST,
    NIGHT
}
