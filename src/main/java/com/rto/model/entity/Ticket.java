package com.rto.model.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Nikita Blyzniukov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "seat")
@Builder
@Entity
@Table(name = "tickets")
public class Ticket implements Serializable {
    private static final long serialVersionUID = 2123464455647322506L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "routeId")
    private Route route;

    @ManyToOne
    @JoinColumn(name = "wagonId")
    private Wagon wagon;

    @OneToOne
    @JoinColumn(name = "seatId")
    private Seat seat;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    private Long cost;
    private String payId;

    @Enumerated(EnumType.STRING)
    private TicketStatus ticketStatus;
}
