package com.rto.model.entity;

public enum RouteStatus {
    PLANNED,
    IN_PROGRESS,
    FINISHED
}
