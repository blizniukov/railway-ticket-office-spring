package com.rto.web.controller.security;

import com.rto.model.entity.User;
import com.rto.model.entity.VerifyToken;
import com.rto.model.service.UserService;
import com.rto.model.service.VerifyTokenService;
import com.rto.model.util.sendgrid.EmailSender;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Optional;

/**
 * @author Nikita Blyzniukov
 */

@Controller
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private static final String INDEX_PAGE = "redirect:/index";

    private final UserService userService;
    private final VerifyTokenService verifyTokenService;
    private final EmailSender emailSender;

    @GetMapping(value = "/signin")
    public String signInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> foundUser = Optional.ofNullable(userService.getByLogin(authentication.getName()));
        if (foundUser.isPresent()) {
            return INDEX_PAGE;
        }
        return "/pages/auth/signin";
    }

    @GetMapping(value = "/signup")
    public String signUpPage() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> foundUser = Optional.ofNullable(userService.getByLogin(authentication.getName()));
        if (foundUser.isPresent()) {
            return INDEX_PAGE;
        }
        return "/pages/auth/signup";
    }

    @PostMapping(value = "/signup")
    public String registerUser(@Valid @RequestParam String login,
                               @RequestParam String password,
                               @Valid @RequestParam String email) {

        if (!userService.isUniqueLogin(login) || userService.isEmailInUse(email)) {
            return "redirect:/auth/signup";
        }

        User user = User.builder()
                .login(login)
                .password(password)
                .email(email)
                .active(false)
                .build();

        VerifyToken verifyToken = userService.createAndGenerateVerifyToken(user);
        emailSender.sendConfirmationEmail(user, verifyToken);

        return "/pages/auth/confirmRegistration";
    }

    @PostMapping("/confirmRegistration")
    public String confirmRegistration(@RequestParam String token,
                                      RedirectAttributes redirectAttributes) {
        VerifyToken verifyToken = verifyTokenService.getByToken(token);

        if (verifyToken.getUser() == null) {
            redirectAttributes.addFlashAttribute("errorConfirmReg", "Error confirm registration");
            return INDEX_PAGE;
        }

        User user = userService.getById(verifyToken.getUser().getId());
        userService.confirmUserAndDeleteToken(user, verifyToken);

        return INDEX_PAGE;
    }

    @GetMapping(value = "/logout")
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }
}
