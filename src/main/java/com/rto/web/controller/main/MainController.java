package com.rto.web.controller.main;

import com.rto.model.entity.Route;
import com.rto.model.entity.RouteStatus;
import com.rto.model.entity.Station;
import com.rto.model.service.RouteService;
import com.rto.model.service.StationService;
import com.rto.model.util.pagination.Paginator;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Controller
@AllArgsConstructor
public class MainController {

    private final StationService stationService;
    private final RouteService routeService;

    @GetMapping("/index")
    public String indexPage(Model model) {
        model.addAttribute("stations", stationService.getAll());
        return "/pages/main/index";
    }

    @PostMapping("/searchRoutes")
    public String mainPage(@PageableDefault(size = 4, sort = "id") Pageable pageable,
                           @RequestParam String departure,
                           @RequestParam String destination,
                           @RequestParam String date,
                           Model model) {

        Station departureStation = stationService.getByName(departure);
        Station destinationService = stationService.getByName(destination);
        LocalDate tripDate = LocalDate.parse(date);
        LocalDateTime ldt = LocalDateTime.of(tripDate, LocalTime.MIN);
        LocalDateTime endDay = LocalDateTime.of(ldt.toLocalDate(), ldt.toLocalTime().plusHours(23));

        Page<Route> page = routeService.searchRoute(departureStation,
                destinationService,
                ldt,
                endDay,
                RouteStatus.FINISHED,
                pageable);

        if (page.isEmpty()) {
            return "redirect:/index?message=No routes found";
        }

        Paginator.paginate(model, page);
        model.addAttribute("page", page);
        return "/pages/main/main";
    }
}
