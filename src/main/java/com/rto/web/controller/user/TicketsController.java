package com.rto.web.controller.user;

import com.rto.model.dto.ApiResponse;
import com.rto.model.dto.ResponseLiqPay;
import com.rto.model.entity.*;
import com.rto.model.service.*;
import com.rto.model.util.liqpay.LiqPayWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.Principal;
import java.util.Map;

/**
 * @author Nikita Blyzniukov
 */
@Controller
@Log4j
@RequestMapping("/tickets")
@AllArgsConstructor
public class TicketsController {

    private RouteService routeService;
    private SeatService seatService;
    private TicketService ticketService;
    private UserService userService;
    private PayService payService;
    private LiqPayWrapper liqPay;

    @PostMapping("/book")
    public String bookTicket(@RequestParam Long seatId,
                             @RequestParam Long routeId,
                             @RequestParam("fname") String firstName,
                             @RequestParam("lname") String lastName,
                             RedirectAttributes redirectAttributes,
                             Principal principal) {

        Route route = routeService.getById(routeId);
        Seat seat = seatService.getById(seatId);
        Wagon wagon = seat.getWagon();
        User user = userService.getByLogin(principal.getName());

        if (routeService.isRouteValidAndSeatFree(route, seat) && user != null) {
            Ticket ticket = Ticket.builder()
                    .firstName(firstName)
                    .lastName(lastName)
                    .route(route)
                    .wagon(wagon)
                    .seat(seat)
                    .user(user)
                    .ticketStatus(TicketStatus.PENDING)
                    .cost(wagon.getSeatCost()).build();
            ticketService.createTicketAndMakeSeatPending(ticket, seat);
            log.debug("Ticket was created");

            redirectAttributes.addAttribute("wagon", wagon.getNumber());
            redirectAttributes.addAttribute("seat", seat.getSeatNumber());
            return "redirect:/tickets/" + ticket.getId() + "/pay";
        }
        return "redirect:/railway";
    }

    @GetMapping("/{ticketId}/pay")
    public String completeBooking(@PathVariable Long ticketId,
                                  @ModelAttribute("wagon") String wagon,
                                  @ModelAttribute("seat") String seat,
                                  Model model) {

        Ticket findTicket = ticketService.getById(ticketId);
        Map<String, String> payRequest = payService.configurePayRequest(findTicket);

        model.addAttribute("wagon", wagon);
        model.addAttribute("seat", seat);
        model.addAttribute("payBtn", liqPay.cnb_form(payRequest));

        return "/pages/main/complete-booking";
    }

    @PostMapping(value = "/checkPayForTicket")
    public String checkPayForTicket(@RequestParam String data,
                                    @RequestParam String signature,
                                    RedirectAttributes redirectAttributes) throws IOException {
        String sign = payService.encodeResponseToBase64(data);

        if (!sign.equals(signature)) {
            log.error("Error pay for ticket");
            redirectAttributes.addAttribute("message", "Error pay for ticket");
            return "redirect:/index";
        }

        String decoded = new String(DatatypeConverter.parseBase64Binary(data));
        ResponseLiqPay responseLiqPay = payService.deserializeResponse(decoded);

        if (payService.isPaySuccess(responseLiqPay.getStatus())) {

            Ticket ticket = ticketService.getByPayId(responseLiqPay.getOrder_id());
            ticketService.bookTicketForSeat(ticket);
        }
        redirectAttributes.addAttribute("message", "Success pay for ticket");
        return "redirect:/user/cabinet/";
    }

    @PostMapping("/return")
    @ResponseBody
    public ResponseEntity<ApiResponse> returnTicket(@RequestParam Long ticketId) {
        Ticket ticket = ticketService.getById(ticketId);

        if (!routeService.isRouteActive(ticket.getRoute()) &&
                !ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {
            log.debug("Unable return ticket");
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Unable return ticket"));
        }

        ticketService.returnTicketAndMakePlaceFree(ticket);
        log.debug("Ticket returned");

        return ResponseEntity.ok(new ApiResponse(true, "Ticket was returned"));
    }
}
