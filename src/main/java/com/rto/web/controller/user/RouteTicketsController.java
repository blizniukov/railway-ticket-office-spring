package com.rto.web.controller.user;

import com.rto.model.dto.SeatsDto;
import com.rto.model.dto.TrainCountSeatsDto;
import com.rto.model.dto.TrainTypeCountDto;
import com.rto.model.entity.*;
import com.rto.model.service.RouteService;
import com.rto.model.service.TrainService;
import com.rto.model.service.WagonService;
import com.rto.model.util.pagination.Paginator;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/routes")
@AllArgsConstructor
@Log4j
public class RouteTicketsController {

    private RouteService routeService;
    private WagonService wagonService;
    private TrainService trainService;

    @GetMapping
    public String allRoutes(@PageableDefault(size = 4, sort = "id") Pageable pageable, Model model) {
        Page<Route> page = routeService.findRoutesAfterDate(LocalDateTime.now(), RouteStatus.FINISHED, pageable);

        Paginator.paginate(model, page);
        model.addAttribute("page", page);
        return "/pages/main/main";
    }

    @GetMapping("/{routeId}/wagontype/{type}/seats")
    public String getRoutePlaces(@PathVariable Long routeId,
                                 @PathVariable String type,
                                 Model model) {
        if (type == null) {
            return "redirect://railway";
        }

        WagonType wagonType = WagonType.valueOf(type);
        Route route = routeService.getById(routeId);
        Train train = route.getTrain();

        TrainCountSeatsDto attributes = wagonService.getWagonsAttributes(train);
        List<Wagon> trainWagons = wagonService.getTrainWagonsOfType(route.getTrain(), wagonType);

        model.addAttribute("route", route);
        model.addAttribute("wagon_types", attributes);
        model.addAttribute("wagons", trainWagons);

        return "/pages/main/ticket-choose";
    }

    @PostMapping("/wagons/{trainId}")
    public ResponseEntity<TrainCountSeatsDto> getWagonPlaces(@PathVariable Long trainId) {

        Train train = trainService.getById(trainId);

        TrainTypeCountDto business = wagonService.getTrainMinCostAndCount(train, WagonType.BUSINESS);
        TrainTypeCountDto economy = wagonService.getTrainMinCostAndCount(train, WagonType.ECONOMY);
        TrainTypeCountDto standard = wagonService.getTrainMinCostAndCount(train, WagonType.STANDARD);

        TrainCountSeatsDto responseEntity = new TrainCountSeatsDto();
        responseEntity.setTrainName(train.getName());
        responseEntity.setTypes(Arrays.asList(business, economy, standard));

        return ResponseEntity.ok(responseEntity);
    }

    @GetMapping("/wagons/{id}/seats")
    public ResponseEntity<SeatsDto> getWagonSeats(@PathVariable Long id) {
        Wagon wagon = wagonService.getById(id);
        SeatsDto seatsDto = new SeatsDto();
        wagon.getSeats().sort(Comparator.comparing(Seat::getSeatNumber));
        seatsDto.setSeats(wagon.getSeats());

        log.debug("Seats was returned");
        return ResponseEntity.ok(seatsDto);
    }
}
