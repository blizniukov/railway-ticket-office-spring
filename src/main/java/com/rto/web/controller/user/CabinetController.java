package com.rto.web.controller.user;

import com.rto.model.entity.Ticket;
import com.rto.model.entity.User;
import com.rto.model.service.TicketService;
import com.rto.model.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/user/cabinet")
@AllArgsConstructor
public class CabinetController {

    private TicketService ticketService;
    private UserService userService;

    @GetMapping({"/", "/history"})
    public String userCabinet(Principal principal, Model model, HttpServletRequest request) {
        User user = userService.getByLogin(principal.getName());

        model.addAttribute("tickets", ticketService.getByUser(user));
        if (request.getRequestURL().toString().contains("history")) {
            return "/pages/cabinet/tickets-history";
        }
        return "/pages/cabinet/cabinet";
    }

    @GetMapping("/{ticketId}")
    public String viewTicket(@PathVariable Long ticketId, Model model) {
        Ticket ticket = ticketService.getById(ticketId);
        String qrCode = ticketService.generateQrCodeByPayId(ticket.getPayId());

        model.addAttribute("ticket", ticket);
        model.addAttribute("qrcode", qrCode);

        return "/pages/cabinet/ticket";
    }
}
