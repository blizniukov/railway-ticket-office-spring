package com.rto.web.controller.user;

import com.rto.model.dto.AjaxLoginEmail;
import com.rto.model.dto.AjaxValidationUser;
import com.rto.model.dto.ApiResponse;
import com.rto.model.entity.User;
import com.rto.model.service.UserService;
import com.rto.model.util.validation.UserValidation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Nikita Blyzniukov
 */

@RestController
@RequestMapping("/user")
@Api(tags = {"AuthController"})
@AllArgsConstructor
public class UserRestController {

    private final UserService userService;
    private final BCryptPasswordEncoder passwordEncoder;

    @ApiOperation(value = "ValidationUserLoginWithAjax", notes = "Make an ajax request to validate User login on exists",
            response = AjaxValidationUser.class)
    @PostMapping("/checkUserLogin")
    public ResponseEntity<AjaxValidationUser> getSearchResultViaAjax(@Valid @RequestBody AjaxValidationUser validationUserBody) {

        AjaxValidationUser result = new AjaxValidationUser();

        if (UserValidation.isLoginBlank(validationUserBody.getLogin())) {
            result.setLogin("invalid input!");
            return ResponseEntity.ok(result);
        }

        User userFromDb = userService.getByLogin(validationUserBody.getLogin());
        boolean checkUserLogin = userService.isUniqueLogin(validationUserBody.getLogin());

        if (userFromDb == null) {
            result.setLogin("no user found!");
            return ResponseEntity.ok().body(result);
        }

        result.setLogin(checkUserLogin ? "no user found!" : "user found!");
        result.setPassword(UserValidation.validatePassword(passwordEncoder, validationUserBody, userFromDb));
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "ValidationUserLoginAndEmail", notes = "Make an ajax request to validate User login and email",
            response = AjaxValidationUser.class)
    @PostMapping("/checkLoginAndEmail")
    public ResponseEntity<ApiResponse> getUsername(@RequestBody AjaxLoginEmail checkInfo) {
        if (!userService.isUniqueLogin(checkInfo.getLogin()) || userService.isEmailInUse(checkInfo.getEmail())) {

            return ResponseEntity.ok(new ApiResponse(false, "Login or email not unique"));
        }

        return ResponseEntity.ok().build();
    }
}
