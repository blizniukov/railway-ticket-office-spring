package com.rto.web.controller.admin.users;

import com.rto.model.entity.User;
import com.rto.model.service.UserService;
import com.rto.model.util.sendgrid.EmailSender;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.constraints.Email;

@Controller
@RequestMapping("/railway/admin/users")
@AllArgsConstructor
@Log4j
public class ManageUsersController {

    private final UserService userService;
    private final EmailSender emailSender;

    @GetMapping("/")
    public String getUsers(Model model) {
        model.addAttribute("users", userService.getAll());
        return "/admin/pages/cabinet/manage-users";
    }

    @PostMapping("/changeUserState/{id}/active/{value}")
    @ResponseBody
    public ResponseEntity<String> changeUserState(@PathVariable Long id,
                                                  @PathVariable Boolean value) {
        User user = userService.getById(id);
        user.setActive(value);
        userService.update(user.getId(), user);
        log.debug("user state was changed");

        return ResponseEntity.ok("State was changed");
    }

    @PostMapping("/sendEmail")
    public String sendEmailToUser(@RequestParam @Email String email,
                                  @RequestParam String title,
                                  @RequestParam String message,
                                  RedirectAttributes redirectAttributes) {
        if (StringUtils.isBlank(email) || StringUtils.isBlank(title) || StringUtils.isBlank(message)) {
            redirectAttributes.addAttribute("message", "Error");
            return "/redirect:/railway/admin/users/";
        }

        emailSender.send(email, message, title);
        redirectAttributes.addAttribute("message", "Email was sent");
        return "redirect:/railway/admin/users/";
    }
}
