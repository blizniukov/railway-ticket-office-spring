package com.rto.web.controller.admin.routes;

import com.rto.model.entity.Route;
import com.rto.model.entity.RouteStatus;
import com.rto.model.entity.Station;
import com.rto.model.entity.Train;
import com.rto.model.service.RouteService;
import com.rto.model.service.StationService;
import com.rto.model.service.TrainService;
import com.rto.model.util.validation.RouteValidation;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;

/**
 * @author Mykyta Blyzniukov
 */
@Controller
@RequestMapping("/railway/admin/routes")
@AllArgsConstructor
public class RoutesController {

    private static final String MESSAGE_ATTRIBUTE = "message";
    private static final String ROUTES_PATH = "redirect:/railway/admin/routes/";

    private RouteService routeService;
    private StationService stationService;
    private TrainService trainService;

    @GetMapping("/")
    public String getRoutes(Model model) {

        model.addAttribute("trains", trainService.getFreeTrains());
        model.addAttribute("stations", stationService.getAll());
        model.addAttribute("routes", routeService.getAll());
        return "/admin/pages/route/routes";
    }

    @PostMapping("/create")
    public String createRoute(@RequestParam String name,
                              @RequestParam String departureDate,
                              @RequestParam String arrivalDate,
                              @RequestParam String status,
                              @RequestParam Long departStationId,
                              @RequestParam Long destinStationId,
                              @RequestParam Long trainId,
                              RedirectAttributes redirectAttributes) {

        LocalDateTime departure = LocalDateTime.parse(departureDate);
        LocalDateTime arrival = LocalDateTime.parse(arrivalDate);
        Station departureStation = stationService.getById(departStationId);
        Station destinationStation = stationService.getById(destinStationId);
        Train train = trainService.getById(trainId);
        Route route = routeService.getByName(name);


        if (RouteValidation.isRouteInputIsNotValid(name, status, departureStation, destinationStation, train, route)) {
            redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Enter valid credentials");
            return ROUTES_PATH;
        }

        if (departure.isAfter(arrival)) {
            redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Departure date can`t be later than arrival");
            return ROUTES_PATH;
        }
        Route routeToSave = Route.builder()
                .name(name)
                .routeStatus(RouteStatus.valueOf(status))
                .departure(departureStation)
                .destination(destinationStation)
                .departureDate(departure)
                .arrivalDate(arrival)
                .train(train)
                .build();
        routeService.create(routeToSave);
        redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Route created");
        return ROUTES_PATH;
    }

    @PostMapping("/update")
    public String updateRoute(@RequestParam Long id,
                              @RequestParam String name,
                              @RequestParam String status,
                              @RequestParam Long departStationId,
                              @RequestParam Long destinStationId,
                              @RequestParam Long trainId,
                              RedirectAttributes redirectAttributes) {
        Route routeToUpdate = routeService.getById(id);
        if (routeToUpdate == null) {
            redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Route is not exists");
            return ROUTES_PATH;
        }

        Station departureStation = stationService.getById(departStationId);
        Station destinationStation = stationService.getById(destinStationId);
        Route route = routeService.getByName(name);

        Train train = trainService.getById(trainId);

        if (RouteValidation.isRouteInputIsNotValid(name, status, departureStation, destinationStation, train, route)) {
            redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Enter valid credentials");
            return ROUTES_PATH;
        }

        routeToUpdate.setName(name);
        routeToUpdate.setRouteStatus(RouteStatus.valueOf(status));
        routeToUpdate.setDeparture(departureStation);
        routeToUpdate.setDestination(destinationStation);
        routeToUpdate.setTrain(train);
        routeService.update(id, routeToUpdate);

        redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Route updated");
        return ROUTES_PATH;
    }
}
