package com.rto.web.controller.admin.stations;

import com.rto.model.entity.Station;
import com.rto.model.service.StationService;
import com.rto.model.util.pagination.Paginator;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

/**
 * @author Mykyta Blyzniukov
 */
@Controller
@RequestMapping("/railway/admin/stations")
@RequiredArgsConstructor
public class StationsController {

    private final StationService stationService;

    private static final String MESSAGE_ATTRIBUTE = "message";
    private static final String STATIONS_PAGE = "redirect:/railway/admin/stations/";

    @GetMapping("/")
    public String stations(@PageableDefault(size = 8, sort = "name") Pageable pageable, Model model) {
        Page<Station> page = stationService.getPage(pageable);
        Paginator.paginate(model, page);

        model.addAttribute("page", page);
        return "/admin/pages/station/stations";
    }


    @PostMapping("/create")
    public String createStation(@RequestParam String name,
                                RedirectAttributes redirectAttributes) {
        Optional<Station> isStationExists = Optional.ofNullable(stationService.getByName(name));
        if (StringUtils.isBlank(name) || isStationExists.isPresent()) {
            redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Station already exists or name is incorrect");
            return STATIONS_PAGE;
        }

        Station station = new Station(name);
        stationService.create(station);
        redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Station was created");
        return STATIONS_PAGE;
    }

    @PostMapping("/update")
    public String updateStation(@RequestParam Long id,
                                @RequestParam String name,
                                RedirectAttributes redirectAttributes) {
        Optional<Station> isNameUnique = Optional.ofNullable(stationService.getByName(name));
        if (StringUtils.isBlank(name) || isNameUnique.isPresent()) {
            redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Station with given name already exists or name invalid");
            return STATIONS_PAGE;
        }

        Station stationToUpdate = stationService.getById(id);
        stationToUpdate.setName(name);
        stationService.update(id, stationToUpdate);
        redirectAttributes.addAttribute(MESSAGE_ATTRIBUTE, "Station was updated");
        return STATIONS_PAGE;
    }
}
