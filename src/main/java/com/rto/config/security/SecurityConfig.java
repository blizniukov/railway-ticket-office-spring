package com.rto.config.security;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public static BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Order(2)
    @Configuration
    @AllArgsConstructor
    public static class MainSecurityConfig extends WebSecurityConfigurerAdapter {
        private static final String[] WHITE_LIST_ROLES = {
                "ADMIN",
                "USER",
        };

        private static final String[] LIST_RESOURCES = {
                "/resources/**",
                "/favicon.ico",
                "/**/*.html",
                "/**/*.css",
                "/**/*.js",
                "/**/*.png",
                "/**/*.gif",
                "/**/*.svg",
                "/**/*.jpg",
                "/favicon.ico"
        };

        private static final String[] LIST_FREE_PATH = {
                "/searchRoutes",
                "/index",
                "/auth/**",
                "/user/**",
                "/routes",
                "/routes/wagons/**"
        };

        private AuthSuccessHandler authSuccessHandler;
        private UserDetailsServiceImpl userDetailsService;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS).and()
                    .authorizeRequests()
                    .antMatchers(LIST_RESOURCES).permitAll()
                    .antMatchers(LIST_FREE_PATH).permitAll()
                    .antMatchers("/railway/admin/**").hasRole("ADMIN")
                    .antMatchers("/**").hasAnyRole(WHITE_LIST_ROLES)
                    .and()
                    .formLogin()
                    .loginPage("/auth/signin")
                    .loginProcessingUrl("/security")
                    .defaultSuccessUrl("/user/cabinet/", true)
                    .successHandler(authSuccessHandler)
                    .failureUrl("/auth/signin?message=User%is%disabled")
                    .usernameParameter("username").passwordParameter("password")
                    .permitAll()
                    .and()
                    .logout()
                    .logoutUrl("/auth/logout")
                    .logoutSuccessUrl("/index")
                    .deleteCookies("JSESSIONID")
                    .invalidateHttpSession(true)
                    .and()
                    .rememberMe()
                    .key("$5keyRTO")
                    .rememberMeParameter("remember-me");
            http
                    .headers()
                    .frameOptions().sameOrigin()
                    .cacheControl();
        }

        @Override
        public void configure(WebSecurity web) {
            web
                    .ignoring()
                    .antMatchers("/h2-console/**/**");
        }

        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder());
        }
    }

    @Order(1)
    @Configuration
    public static class SwaggerSecurityConfiguration extends WebSecurityConfigurerAdapter {
        private static final String[] AUTH_WHITELIST = {
                // -- swagger ui
                "/configuration/ui",
                "/configuration/security",
                "/swagger-ui.html",
                "/swagger-resources",
                "/webjars/**"
        };

        private static final String[] AUTH_ALL_LIST = {
                "/swagger-resources/configuration/ui",
                "/swagger-resources/configuration/security",
                "/v2/api-docs"
        };

        private UserDetailsServiceImpl userDetailsService;

        @Autowired
        public SwaggerSecurityConfiguration(UserDetailsServiceImpl userDetailsService) {
            this.userDetailsService = userDetailsService;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/swagger**")
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                    .httpBasic().and()
                    .authorizeRequests()
                    .antMatchers(AUTH_WHITELIST).authenticated();
        }

        @Override
        public void configure(WebSecurity web) {
            web
                    .ignoring()
                    .antMatchers(AUTH_ALL_LIST);
        }

        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder());
        }
    }

    @Bean
    public SpringSecurityDialect securityDialect() {
        return new SpringSecurityDialect();
    }
}
