package com.rto.model.service.implementation;

import com.rto.model.entity.Role;
import com.rto.model.entity.RoleName;
import com.rto.model.service.RoleService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class RoleServiceImplTest {

    @Autowired
    private RoleService roleService;

    @Before
    public void initRole() {
        Role role = Role.builder().name(RoleName.ROLE_ADMIN).build();
        if (roleService.getAll().isEmpty()){
            roleService.create(role);
        }
    }

    @Test
    public void getByName() {
        Role role = roleService.getByName(RoleName.ROLE_ADMIN);
        Assert.assertEquals("ROLE_ADMIN", role.getName().toString());
    }

    @Test
    public void getAll() {
        Assert.assertFalse(roleService.getAll().isEmpty());
    }
}