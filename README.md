# Railway Ticket Office

![](gitlab-images/index.png)
**Images:**
<details>
<summary>Click to see images</summary>
<img src="gitlab-images/signin.png" alt="SignIn page">
<img src="gitlab-images/signup.png" alt="SignUp page">

## Choose route
<img src="gitlab-images/routes.png" alt="Routes page">

## Choose wagon and place for booking
<img src="gitlab-images/choose-ticket.png" alt="Choose ticket page">

## Confirm booking and go to pay page
<img src="gitlab-images/confirm.png" alt="Confirm booking page">

## PrivatBank pay page
<img src="gitlab-images/pay.png" alt="Routes page">

## Cabinet
<img src="gitlab-images/cabinet-tickets.png" alt="User cabinet page">

## Single ticket with generated QR code
<img src="gitlab-images/cabinet-ticket.png" alt="Single ticket with QR">

## Manage routes (create, update)
<img src="gitlab-images/manage-routes.png" alt="Manage routes page">

## Page to manege users(disable user or send email)
<img src="gitlab-images/manage-users.png" alt="Manage users page">
</details>

***
Online train ticket office for the purchase of train tickets.
There are many options for user to book ticket for route. Ticket office can be easily extened for sale bus tickets and etc. User can choose tickets, see free or booked places, pay for ticket, return ticket, print ticket and much more.


**Features:**
*  Registration with email confirmation.
*  Send email function (from admin to user).
*  Book ticket option and integration with PrivatBank API (user can pay with Credit Card).
*  User cabinet to manage tickets.
*  User can see ticket in cabinet with generated QR-code to access train.
*  Administrator can disable users(disabled users can`t login).
*  Search routes feature.
*  Pagination and sorting

**Technologies:**
*  Spring Boot 2.0
*  Spring Data JPA
*  Spring Security
*  Spring MVC
*  Spring REST
*  SendGrid
*  LiqPay API
*  Zxing
*  Swagger 2
*  Flyway
*  Lombok
*  Apache Commons
*  JQuery/JavaScript
*  HTML5, SCSS
*  Thymeleaf
*  Log4J
*  PostgreSQL

**Installation instructions:**
1. Clone repository.
2. Create database rtospring in PostgreSQL. Change database login and password in application.yml config.
3. Change route dates at db/migration/V5_1__insert_data_into_routes.sql or create new ones using http://localhost:8080/railway/admin/routes/
4. Run application. Flyway will create tables and fill them.
5. Login using one of two test users or register new one. 

**There are two test users:**
* blizniukov - 123456 (Administrator)
* kkkkkk - 123456 (User)